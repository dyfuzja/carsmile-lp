<?php

include('_carSettings.php');
   $tpl = '.';

   $carNAME = "Nissan<br>Qashqai";
   $carNameForm = "Nissan Qashqai";
   $carPRICE = 745;
   $promoImg = "/dest/images/promo_qashqai.png";
   $promoSmallImg = "/dest/images/img_qashqai.jpg";
    ?>
<!DOCTYPE html>
<html lang="pl">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
      <title>Carsmile</title>
      <link href="dest/css/app.css" rel="stylesheet" type="text/css" />
      <link rel="shortcut icon" href="https://carsmile.pl/img/favicon/favicon.ico">
       <script>

        //  localStorage.clear();


    //      const ob = {
    // 'carBrand': 'Renault',
    // 'carType': 'Clio',
    // 'carBrandTypeID': '131492-1'
// };

         dataLayer = window.dataLayer || [{
             'st.pageCategory':'LandingPage',
             'st.desc':'contact data'
             // 'offerType':<dla-mnie|dla-firmy>,
             // 'financeType':<najem|leasing>,
             // 'carCategory': '???',
            //  'carBrand': ob.carBrand,
            //  'carType': ob.carType,
            //  'carBrandTypeID': ob.carBrandTypeID
         }];

// localStorage.setItem('carBrand', ob.carBrand);
// localStorage.setItem('carType', ob.carType);
// localStorage.setItem('carBrandTypeID', ob.carBrandTypeID);


</script>
     <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PC37ZJK');</script>
<!-- End Google Tag Manager -->
   </head>
   <body>
   <form id="LPform" action="./">


      <!-- Google Tag Manager (noscript) -->
      <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PC37ZJK"
         height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
     <!-- End Google Tag Manager (noscript) -->
         <input type="hidden" name="selectedcar" value="<?= $carNameForm ?>" />
         <header class="Header" >
            <div class="row">
               <div class="grid-x grid-padding-x align-middle phone-reverse">
                     <a href="#">
                     <img src="dest/images/logo.svg" alt="" class="Header__logoImg" />
                     </a>
               </div>
            </div>
         </header>
         <div class="main-wrapper">

            <div class="row main">
               <div class="grid-x grid-padding-x PhoneMarginMinus">
                  <div class="cell large-12 speciallarge-8 carSlideImage">
                     <div class="mobile-text-header">
                     <h1 class="header-main-text"><span>Znajdź swój wymarzony samochód bez wychodzenia z domu</span></h1>
                     <ul class="header-benefits">
                       <li>Oferujemy samochody w najmie długoterminowym,
                         <span>w opłacalnym abonamencie.</span>
                       </li>
                       <li>Przygotowujemy ofertę na każdy dostępny
                         <span>w Polsce model samochodu.</span></li>
                       <li>W ramach Raty Wszystkomającej oferujemy:
                         <span>serwis, naprawy, ubezpieczenie i dwa komplety opon (letnie i zimowe).</span>
                       </li>
                     </ul>
                     </div>


                     <div class="switch-wrapper">
                     <div class="switch-text">Ile chcesz zapłacić za swój samochód?</div>
                        <div class="switch-btn-wrapper">
                        <button class="switch-btn btn-choice active-button" data-range="1000">1000</button>
                        <button class="switch-btn btn-choice" data-range="1500">1500</button>
                        <button class="switch-btn btn-choice" data-range="2000">2000</button>
                        <button class="switch-btn btn-choice" data-range="2500">2500</button>
                        <button class="switch-btn btn-choice" data-range="max">max</button>
                        </div>
                      </div>

                      <div class="offers-wrapper offers-1000 offer-range-display" data-offers-range="1000" >
                      <div class="offer-box"  data-price-query="57b0afac32a6a02fee1aea765d25388a" data-car-type-code="108839-1">
                        <div class="offer-img-wrapper">
                        <img src="dest/images/corsa.jpg" alt="">
                        </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>OPEL Corsa</h3>
                        <p>OPEL Corsa Hatchback
                        Corsa 1.4 Enjoy
                        75KM, 1.4L, 4 cylindry, Manualna, Napęd przedni</p>
                        </div>
                        <div class="price constant-price">
                          <span class="price-value">798</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                        <div class="price-km">
                          <span class="price-value">26</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>
                      <div class="offer-box" data-price-query="7a993d55104a169837e837fa97047d57" data-car-type-code="126654-1">

                      <div class="offer-img-wrapper">
                      <img src="dest/images/fabia.jpg" alt="Skoda Fabia">
                        </div>
                    <div class="offer-details">
                      <div class="car-info">
                      <h3>SKODA Fabia</h3>
                      <p>SKODA Fabia Hatchback
Fabia 1.0 Ambition
75KM, 1L, 3 cylindry, Manualna, Napęd przedni</p>
                      </div>
                      <div class="price constant-price">
                        <span class="price-value">840</span>
                        <span class="currency"><span class="bold">pln* </span>/mies.</span>
                      </div>
                      <span class="plus">+</span>
                        <div class="price-km">
                          <span class="price-value">25</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                  </div>
                    </div>
                    <div class="offer-box" data-price-query="3b3f90d6b5a7a3a6cc844b29bc8939f5" data-car-type-code="122882-1">
                    <div class="offer-img-wrapper">
                        <img src="dest/images/logan.jpg" alt="">
                    </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>DACIA Logan</h3>
                        <p>DACIA Logan Sedan
                        Logan 0.9 TCe Laureate S&S
                        </p>
                        </div>
                        <div class="price constant-price">
                          <span class="price-value">898</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                        <div class="price-km">
                          <span class="price-value">32</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>
                      <div class="offer-box" data-price-query="719f01511a8cb3d5ef55fe9918f769e6" data-car-type-code="126846-1">
                      <div class="offer-img-wrapper">
                        <img src="dest/images/qashqai.jpg" alt="">
      </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>NISSAN Qashqai</h3>
                        <p>NISSAN Qashqai Crossover
                        Qashqai 1.2</p>
                        </div>
                        <div class="price constant-price">
                          <span class="price-value">956</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                        <div class="price-km">
                          <span class="price-value">28</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>
                    </div>



                    <div class="offers-wrapper offers-1500" data-offers-range="1500">
                        <div class="offer-box" data-price-query="fd006abe24425d14703bd5d70639ab90" data-car-type-code="128051-1">
                      <div class="offer-img-wrapper">
                          <img src="dest/images/aircross.jpg" alt="">
      </div>
                        <div class="offer-details">
                          <div class="car-info">
                          <h3>CITROEN C3 Aircross</h3>
                          <p>CITROEN C3 Aircross Crossover
                          C3 Aircross 1.2 PureTech Feel S&S</p>
                          </div>
                          <div class="price constant-price">
                            <span class="price-value">1044</span>
                            <span class="currency"><span class="bold">pln* </span>/mies.</span>
                          </div>
                        <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">28</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                      </div>
                        </div>
                        <div class="offer-box" data-price-query="5c729050a7abf9e637b71c3c5452784f" data-car-type-code="109196-1">
                      <div class="offer-img-wrapper">
                        <img src="dest/images/passat.jpg" alt="">
      </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>VOLKSWAGEN Passat</h3>
                        <p>VOLKSWAGEN Passat Sedan
Passat 1.4 TSI BMT Comfortline</p>
                        </div>
                        <div class="price constant-price">
                          <span class="price-value">1213</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">42</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>
                      <div class="offer-box" data-price-query="3e710da259b75cd00300c58aa6c1b0c1" data-car-type-code="124991-1">
                      <div class="offer-img-wrapper">
                          <img src="dest/images/insignia.jpg" alt="">
      </div>
                        <div class="offer-details">
                          <div class="car-info">
                          <h3>OPEL Insignia</h3>
                          <p>OPEL Insignia Sedan
                          Insignia 1.5 T Innovation S&S</p>
                          </div>
                          <div class="price constant-price">
                            <span class="price-value">1316</span>
                            <span class="currency"><span class="bold">pln* </span>/mies.</span>
                          </div>
                          <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">36</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                      </div>
                        </div>
                        <div class="offer-box" data-price-query="a7e27b17c2ef57a753548e2ef54a7472" data-car-type-code="114603-1">
                      <div class="offer-img-wrapper">
                          <img src="dest/images/bmw-3.jpg" alt="">
      </div>
                        <div class="offer-details">
                          <div class="car-info">
                          <h3>BMW Seria 3</h3>
                          <p>BMW Seria 3 Sedan
                          318i Advantage aut</p>
                          </div>
                          <div class="price constant-price">
                            <span class="price-value">1419</span>
                            <span class="currency"><span class="bold">pln* </span>/mies.</span>
                          </div>
                          <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">49</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                      </div>
                        </div>
                      </div>


                      <div class="offers-wrapper offers-2000" data-offers-range="2000">
                        <div class="offer-box" data-price-query="631b17f8a25204dd294e8648b8f30c89" data-car-type-code="121265-1">
                      <div class="offer-img-wrapper">
                          <img src="dest/images/giulia.jpg" alt="">
      </div>
                        <div class="offer-details">
                          <div class="car-info">
                          <h3>ALFA ROMEO Giulia</h3>
                          <p>ALFA ROMEO Giulia Sedan
                          Giulia 2.0 Turbo aut</p>
                          </div>
                          <div class="price constant-price">
                            <span class="price-value">1780</span>
                            <span class="currency"><span class="bold">pln* </span>/mies.</span>
                          </div>
                          <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">52</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                      </div>
                        </div>
                        <div class="offer-box" data-price-query="4ccef3b2ff9c3e91d422440cdf2739e9" data-car-type-code="127626-1">
                      <div class="offer-img-wrapper">
                          <img src="dest/images/xtrail.jpg" alt="">
      </div>
                        <div class="offer-details">
                          <div class="car-info">
                          <h3>NISSAN X-Trail</h3>
                          <p>NISSAN X-Trail SUV
                          X-Trail 2.0 dCi Tekna 4WD Xtronicc</p>
                          </div>
                          <div class="price constant-price">
                            <span class="price-value">1802</span>
                            <span class="currency"><span class="bold">pln* </span>/mies.</span>
                          </div>
                          <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">64</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                      </div>
                        </div>
                        <div class="offer-box constant-price" data-price-query="cb3c10abe74b33bf9450bb9db12939fd" data-car-type-code="125594-2">
                      <div class="offer-img-wrapper">
                        <img src="dest/images/bmw4.jpg" alt="">
      </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>BMW Seria 4</h3>
                        <p>BMW Seria 4 Coupe
420i Advantage aut</p>
                        </div>
                        <div class="price constant-price">
                          <span class="price-value">1871</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">67</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>

                        <div class="offer-box" data-price-query="dab93d13539e00ffe9f9733e0bf07b2c" data-car-type-code="118400-1">
                      <div class="offer-img-wrapper">
                          <img src="dest/images/a4.jpg" alt="">
      </div>
                        <div class="offer-details">
                          <div class="car-info">
                          <h3>AUDI A4 Kombi</h3>
                          <p>AUDI A4 Kombi
                          A4 2.0 TDI S tronic</p>
                          </div>
                          <div class="price constant-price">
                            <span class="price-value">1955</span>
                            <span class="currency"><span class="bold">pln* </span>/mies.</span>
                          </div>
                          <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">58</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                      </div>
                        </div>
                      </div>


                      <div class="offers-wrapper offers-2500" data-offers-range="2500">

                    <div class="offer-box" data-price-query="0ec16264758e0152157a5524d37342d4" data-car-type-code="123783-1">
                    <div class="offer-img-wrapper">
                          <img src="dest/images/cclass.jpg" alt="">
      </div>
                        <div class="offer-details">
                          <div class="car-info">
                          <h3>MERCEDES-BENZ C Klasa</h3>
                          <p>MERCEDES-BENZ C Klasa Kombi
                          C 200 9G-TRONIC</p>
                          </div>
                          <div class="price constant-price">
                            <span class="price-value">2055</span>
                            <span class="currency"><span class="bold">pln* </span>/mies.</span>
                          </div>
                          <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">75</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                      </div>
                        </div>
                        <div class="offer-box" data-price-query="2a3d12c9701dfbb1c66e8391d6bc7c99" data-car-type-code="112636-1">
                      <div class="offer-img-wrapper">
                      <img src="dest/images/xe.jpg" alt="">
      </div>
                    <div class="offer-details">
                      <div class="car-info">
                      <h3>JAGUAR XE</h3>
                      <p>JAGUAR XE Sedan
                      XE 2.0 D Prestige aut</p>
                      </div>
                      <div class="price constant-price">
                        <span class="price-value">2067</span>
                        <span class="currency"><span class="bold">pln* </span>/mies.</span>
                      </div>
                      <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">60</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                  </div>
                    </div>
                        <div class="offer-box" data-price-query="f4f300c358cd4ec85edd30bf66331a0a" data-car-type-code="131016-1">
                      <div class="offer-img-wrapper">
                        <img src="dest/images/x3.jpg" alt="">
      </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>BMW X3</h3>
                        <p>BMW X3 SUV
X3 xDrive20d Advantage aut</p>
                        </div>
                        <div class="price constant-price">
                          <span class="price-value">2109</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">75</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>

                        <div class="offer-box" data-price-query="0e4397f5d3beccf0a8d25f149b5d20f8" data-car-type-code="127871-1">
                      <div class="offer-img-wrapper">
                          <img src="dest/images/stinger.jpg" alt="">
      </div>
                        <div class="offer-details">
                          <div class="car-info">
                          <h3>KIA Stinger</h3>
                          <p>KIA Stinger Sedan
                          Stinger 2.0 T-GDI XL</p>
                          </div>
                          <div class="price constant-price">
                            <span class="price-value">2269</span>
                            <span class="currency"><span class="bold">pln* </span>/mies.</span>
                          </div>
                          <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">74</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                      </div>
                        </div>
                      </div>


                      <div class="offers-wrapper offers-max" data-offers-range="max">
                        <div class="offer-box" data-price-query="ce7766500c65cb4dac7e60385b78f707" data-car-type-code="118558-1">
                      <div class="offer-img-wrapper">
                          <img src="dest/images/macan.jpg" alt="">
      </div>
                        <div class="offer-details">
                          <div class="car-info">
                          <h3>PORSCHE Macan</h3>
                          <p>Crossover
                          Macan
                          252KM, 2L, 4 cylindry, Automatyczna, Stały napęd na 4 koła off road</p>
                          </div>
                          <div class="price constant-price">
                            <span class="price-value">3014</span>
                            <span class="currency"><span class="bold">pln* </span>/mies.</span>
                          </div>
                          <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">91</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                      </div>
                        </div>
                        <div class="offer-box" data-price-query="48e0a7359175d1c8e9d63effb4ddb9ed" data-car-type-code="130852-1">
                      <div class="offer-img-wrapper">
                        <img src="dest/images/xc60.jpg" alt="">
      </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>VOLVO XC 60</h3>
                        <p>VOLVO XC 60 SUV
                        XC 60 D4 AWD Inscription aut</p>
                        </div>
                        <div class="price constant-price">
                          <span class="price-value">2625</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">78</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>
                      <div class="offer-box" data-price-query="0d47a5518250d376a9a33130377d9333" data-car-type-code="122970-1">
                      <div class="offer-img-wrapper">
                          <img src="dest/images/discovery.jpg" alt="">
      </div>
                        <div class="offer-details">
                          <div class="car-info">
                          <h3>Range Rover Discovery</h3>
                          <p>LAND ROVER Discovery SUV
                          Discovery V 2.0 SD4 SE</p>
                          </div>
                          <div class="price constant-price">
                            <span class="price-value">2823</span>
                            <span class="currency"><span class="bold">pln* </span>/mies.</span>
                          </div>
                          <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">86</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                      </div>
                        </div>
                        <div class="offer-box" data-price-query="5be0604bd27506d5c45f6384d46e8f61" data-car-type-code="123648-1">
                      <div class="offer-img-wrapper">
                          <img src="dest/images/bmw-5.jpg" alt="">
      </div>
                        <div class="offer-details">
                          <div class="car-info">
                          <h3>BMW Seria 5 Sedan</h3>
                          <p>BMW Seria 5 Sedan
                          520d xDrive aut</p>
                          </div>
                          <div class="price constant-price">
                            <span class="price-value">2581</span>
                            <span class="currency"><span class="bold">pln* </span>/mies.</span>
                          </div>
                          <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">81</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                      </div>
                        </div>
                      </div>



                  </div>
                  <div class="cell large-12 speciallarge-4 padding-less speciallarge relative" style="position: relative;">
                      <img src="dest/images/shape-form.svg" class="bg-image" alt="">
                    <div class="Box">
                    <div class="showbox" style="display: none">
                            <div class="loader"></div>
                            <p class="loading_info">Trwa przesyłanie formularza</p>
                        </div>
                        <div class="success" style="display: none">
                            <img src="<?= $tpl ?>/dest/images/tickYES.png" alt="" class="successForm">
                            <p class="loading_info successInfo"><h3>Dziękujemy.</h3> <br/>Skontaktujemy się z Tobą wkrótce.</p>
                        </div>
                           <div class="Form">
                              <p class="headtext">Wypełnij formularz</p>
                              <p class="undertext">i dowiedz się, jak tani może być nowy samochód!</p>
                              <div class="Form-wrapper">
                                 <div class="Form__input-wrapper">
                                    <input autocomplete="off" type="text" name="client_name" class="Form__input client_name">
                                    <label class="Form__input-label client_name-label">Imię</label>
                                    <img src="<?= $tpl ?>/dest/images/tickYES.svg" alt="" class="inputValid">
                                 </div>
                                 <div class="Form__input-wrapper">
                                          <input autocomplete="off" type="text" name="client_email" class="Form__input client_email">
                                          <label class="Form__input-label client_email-label">E-mail</label>
                                          <img src="<?= $tpl ?>/dest/images/tickYES.svg" alt="" class="inputValid">
                                       </div>
                                       <div class="Form__input-wrapper">
                                          <input autocomplete="off" type="text" name="client_mobile" class="Form__input client_mobile">
                                          <label class="Form__input-label client_mobile-label">Telefon</label>
                                          <img src="<?= $tpl ?>/dest/images/tickYES.svg" alt="" class="inputValid">
                                       </div>
                                       <div class="Form__input-wrapper">
                                       <select name="client_choice" class="client_choice">
                                       <option disabled selected value>Wybierz samochód</option>
                                       <option value="121265-1">ALFA ROMEO Giulia</option>
                                       <option value="118400-1">AUDI A4 Kombi</option>
                                       <option value="130563-1">AUDI A7 Sedan</option>
                                       <option value="114603-1">BMW Seria 3</option>
                                       <option value="125594-2">BMW Seria 4</option>
                                       <option value="123648-1">BMW Seria 5</option>
                                       <option value="131016-1">BMW X3</option>
                                       <option value="128051-1">CITROEN C3 Aircross</option>
                                       <option value="122882-1">DACIA Logan</option>
                                       <option value="112636-1">JAGUAR XE</option>
                                       <option value="127871-1">KIA Stinger</option>
                                       <option value="123783-1">MERCEDES-BENZ C Klasa</option>
                                       <option value="126846-1">NISSAN Qashqai</option>
                                       <option value="127626-1">NISSAN X-Trail</option>
                                       <option value="108839-1">OPEL Corsa</option>
                                       <option value="124991-1">OPEL Insignia</option>
                                       <option value="118558-1">PORSCHE Macan</option>
                                       <option value="122970-1">RANGE ROVER DISCOVERY</option>
                                       <option value="126654-1">SKODA Fabia</option>
                                       <option value="109196-1">VOLKSWAGEN Passat</option>
                                       <option value="130852-1">VOLVO XC 60</option>
                                       </select>
                                          <img src="<?= $tpl ?>/dest/images/tickYES.svg" alt="" class="inputValid">
                                       </div>

                                 <div class="grid-x grid-margin-x check-company-normalize">
                                    <div class="cell xphone-auto Agrees terms-align agreeAndButton">
                                       <div class="Form__input-wrapper terms company-check Agrees">
                                          <!-- <label for="checkNIP" class="checkNIP-label">
                                             <span class="checkbox checkbox-large"><span class="checked"></span></span> -->


                                             <!-- <input type="checkbox" hidden="hidden" id="username" value="test"> -->

                                          <input type="checkbox" id="checkNIP" name="checkNIP" value="true">


                                             <label class="switch" for="checkNIP" class="checkNIP-label"></label>


                                             <span class="terms-text company-terms-text nip-padd">Prowadzisz firmę?</span>
                                             <div class="error formFirst"></div>
                                          </label>
                                       </div>
                                    </div>
                                    <div class="cell xphone-auto lessLeft nip-normalize">
                                       <div class="Form__input-wrapper">
                                          <input autocomplete="off" type="text" name="client_nip" class="Form__input client_nip" id="client_nip">
                                          <label class="Form__input-label client_nip_label hide-label">NIP</label>
                                          <img src="<?= $tpl ?>/dest/images/tickYES.svg" alt="" class="inputValid">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="agreeAndButton">
                                    <div class="Agrees">
                                       <div class="cell auto">
                                          <div class="terms check-all-wrapper">
                                             <div class="company-check">
                                                <!-- <input type="checkbox" id="checkAll" name="zaznaczAll" value="true">
                                                <label for="checkAll"> -->


                                                <input type="checkbox" id="checkAll" name="checkAll" value="true">
                                             <label class="switch switch-agrees" for="checkAll" class="checkNIP-label"></label>


<!-- <span class="checkbox checkbox-large"><span class="checked"></span></span> -->

                                                   <span class="terms-text"><b>Zaznacz wszystkie zgody.</b></span>
                                                   <div class="error formFirst"></div>
                                                </label>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <button type="submit" class="Form__SendBtn">
                                    Wyślij
                                    </button>
                                 </div>
                              </div>
                           </div>
      <!-- </form> -->
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="Agrees Agrees__container BoxWithShadow BoxShadowAgrees">
            <div class="formSendContainer" style="display: none"></div>
            <div class="row">
               <div class="grid-x grid-padding-x">
                  <div class="cell xphone-4">
                     <div class="terms">
                        <input type="checkbox" agree="general" id="form_agree1" name="form_agree1" value="true" <?= $agreements[6]['required'] ? 'required' : '' ?>>
                        <label for="form_agree1">
                           <span class="checkbox"><span class="checked"></span></span>
                           <span class="terms-text"><?= $agreements[6]['content'] ?></span>
                           <div class="error errorPlacement"></div>
                        </label>
                     </div>
                     <div class="terms">
                        <input type="checkbox" agree="general" id="form_agree2" name="form_agree2" value="true" <?= $agreements[7]['required'] ? 'required' : '' ?>>
                        <label for="form_agree2">
                           <span class="checkbox"><span class="checked"></span></span>
                           <span class="terms-text"><?= $agreements[7]['content'] ?></span>
                           <p class="error errorPlacement"></p>
                        </label>
                     </div>
                  </div>
                  <div class="cell xphone-4">
                     <div class="terms">
                        <input type="checkbox" agree="general" id="form_agree3" name="form_agree3" value="true" <?= $agreements[8]['required'] ? 'required' : '' ?>>
                        <label for="form_agree3">
                           <span class="checkbox"><span class="checked"></span></span>
                           <span class="terms-text"><?= $agreements[8]['content'] ?></span>
                           <p class="error errorPlacement"></p>
                        </label>
                     </div>
                  </div>
                  <div class="cell xphone-4">
                     <div class="terms">
                        <input type="hidden" name="form_agree4" value="false">
                        <input type="checkbox" agree="general" id="form_agree4" name="form_agree4" value="true" <?= $agreements[9]['required'] ? 'required' : '' ?>>
                        <label for="form_agree4">
                           <span class="checkbox"><span class="checked"></span></span>
                           <span class="terms-text"><?= $agreements[9]['content'] ?></span>
                           <p class="error errorPlacement"></p>
                        </label>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="Rodo_disclaimer">
                  <div class="grid-x grid-padding-x">
                     <div class="cell auto rodo_text">
                        <?= $agreements[10]['content'] ?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <footer class="Footer">
            <div class="row">
               <div class="grid-x grid-padding-x">
                  <div class="cell">
                     <div class="menu-block">
                        <p class="title">Carsmile <span class="copyright">© 2018</span></p>
                        <p class="Footer-disclaimer">
                           Carsmile Spółka Akcyjna z siedzibą w Warszawie, ul. Prosta 51, 00-838 Warszawa, wpisana do rejestru przedsiębiorców prowadzonego przez Sąd Rejonowy dla m.st. Warszawy w Warszawie, XII Wydział Gospodarczy Krajowego Rejestru Sądowego, pod numerem KRS 0000724919, REGON 369367192, NIP 7010800013.
                        </p>
                        <p class="Footer-disclaimer">
                        Złożenie przez nas ostatecznej oferty zależy od Twojej zdolności do płacenia abonamentu obliczonej m.in. w oparciu o Twoje zarobki, zobowiązania oraz historię finansową.
                        </p>
                        <p class="Footer-disclaimer">
                        *Ostateczna wysokość czynszu może się różnić w zależności od wybranego modelu samochodu, uzgodnionego wkładu własnego oraz okresu najmu.
                        </p>
                     </div>
                  </div>
               </div>
            </div>
         </footer>
      </form>

      <!-- All your html code -->
      <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
      <script src="<?= $tpl ?>/dest/js/validate.js"></script>
      <script src="<?= $tpl ?>/dest/js/main.js"></script>
   </body>
</html>
