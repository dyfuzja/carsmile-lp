<?php

include('_carSettings.php');
   $tpl = '.';

   $carNAME = "Nissan<br>Qashqai";
   $carNameForm = "Nissan Qashqai";
   $carPRICE = 745;
   $promoImg = "/dest/images/promo_qashqai.png";
   $promoSmallImg = "/dest/images/img_qashqai.jpg";
    ?>
<!DOCTYPE html>
<html lang="pl">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
      <title>Carsmile</title>
      <link href="dest/css/app.css" rel="stylesheet" type="text/css" />
      <link rel="shortcut icon" href="https://carsmile.pl/img/favicon/favicon.ico">
       <script>

        //  localStorage.clear();


    //      const ob = {
    // 'carBrand': 'Renault',
    // 'carType': 'Clio',
    // 'carBrandTypeID': '131492-1'
// };

         dataLayer = window.dataLayer || [{
             'st.pageCategory':'LandingPage',
             'st.desc':'contact data'
             // 'offerType':<dla-mnie|dla-firmy>,
             // 'financeType':<najem|leasing>,
             // 'carCategory': '???',
            //  'carBrand': ob.carBrand,
            //  'carType': ob.carType,
            //  'carBrandTypeID': ob.carBrandTypeID
         }];

// localStorage.setItem('carBrand', ob.carBrand);
// localStorage.setItem('carType', ob.carType);
// localStorage.setItem('carBrandTypeID', ob.carBrandTypeID);


</script>
     <!-- Google Tag Manager
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PC37ZJK');</script>
End Google Tag Manager -->
   </head>
   <body>
   <form id="LPform" action="./">


      <!-- Google Tag Manager (noscript) -->
      <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PC37ZJK"
         height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
     <!-- End Google Tag Manager (noscript) -->
         <input type="hidden" name="selectedcar" value="<?= $carNameForm ?>" />
         <header class="Header" >
            <div class="row">
               <div class="grid-x grid-padding-x align-middle phone-reverse">
                     <a href="#">
                     <img src="dest/images/logo.svg" alt="" class="Header__logoImg" />
                     </a>
               </div>
            </div>
         </header>
         <div class="main-wrapper">
         <div class="head-wrapper">
         <div class="row main">
               <div class="grid-x grid-padding-x PhoneMarginMinus">
                  <div class="cell large-12 speciallarge-8 carSlideImage">
                     <div class="mobile-text-header">
                     <h1 class="header-main-text"><span>Najlepsze oferty Carsmile</span></h1>
                     </div>
                     </div>
                     </div>
                     </div>
                     </div>

            <div class="row main">
               <div class="grid-x grid-padding-x PhoneMarginMinus">
                  <div class="cell large-12 speciallarge-8 carSlideImage">
                     <div class="switch-wrapper">
                     <div class="switch-text">Ile chcesz zapłacić za swój samochód?</div>
                        <div class="switch-btn-wrapper">
                        <button class="switch-btn btn-choice active-button" data-range="1000">1000</button>
                        <button class="switch-btn btn-choice" data-range="1500">1500</button>
                        <button class="switch-btn btn-choice" data-range="2000">2000</button>
                        <button class="switch-btn btn-choice" data-range="2500">2500</button>
                        <button class="switch-btn btn-choice" data-range="max">max</button>
                        </div>
                      </div>

                      <div class="offers-wrapper offers-1000 offer-range-display" data-offers-range="1000" >
                      <div class="offer-box hot-deal" data-car-type-code="108839-1">
                        <div class="offer-img-wrapper">
                        <img src="dest/images/corsa.jpg" alt="">
                        </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>OPEL Corsa</h3>
                        <p>OPEL Corsa Hatchback
                        Corsa 1.4 Enjoy
                        75KM, 1.4L, 4 cylindry, Manualna, Napęd przedni</p>
                        </div>
                        <div class="price">
                          <span class="price-value">798</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                        <div class="price-km">
                          <span class="price-value">26</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>
                      <div class="offer-box hot-deal" data-car-type-code="126654-1">

                      <div class="offer-img-wrapper">
                      <img src="dest/images/fabia.jpg" alt="Skoda Fabia">
                        </div>
                    <div class="offer-details">
                      <div class="car-info">
                      <h3>SKODA Fabia</h3>
                      <p>SKODA Fabia Hatchback
Fabia 1.0 Ambition
75KM, 1L, 3 cylindry, Manualna, Napęd przedni</p>
                      </div>
                      <div class="price">
                        <span class="price-value">840</span>
                        <span class="currency"><span class="bold">pln* </span>/mies.</span>
                      </div>
                      <span class="plus">+</span>
                        <div class="price-km">
                          <span class="price-value">25</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                  </div>
                    </div>
                    <div class="offer-box hot-deal" data-car-type-code="130696-2">
                    <div class="offer-img-wrapper">
                        <img src="dest/images/focus.jpg" alt="">
                    </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>FORD Focus</h3>
                        <p>FORD Focus Hatchback
                        Focus 1.0 EcoBoost Trend
                        </p>
                        </div>
                        <div class="price">
                          <span class="price-value">882</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                        <div class="price-km">
                          <span class="price-value">24</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>
                      <div class="offer-box hot-deal" data-car-type-code="126846-1">
                      <div class="offer-img-wrapper">
                        <img src="dest/images/qashqai.jpg" alt="">
      </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>NISSAN Qashqai</h3>
                        <p>NISSAN Qashqai Crossover
                        Qashqai 1.2</p>
                        </div>
                        <div class="price">
                          <span class="price-value">956</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                        <div class="price-km">
                          <span class="price-value">28</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>
                      <div class="offer-box hot-deal" data-car-type-code="122551-1">
                        <div class="offer-img-wrapper">
                        <img src="dest/images/mokka.jpg" alt="">
                        </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>OPEL Mokka</h3>
                        <p>OPEL Mokka Crossover
                        Mokka X 1.4 T Elite S&S</p>
                        </div>
                        <div class="price">
                          <span class="price-value">813</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                        <div class="price-km">
                          <span class="price-value">24</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>

                      <div class="offer-box" data-car-type-code="109899-1">
                      <div class="offer-img-wrapper">
                      <img src="dest/images/i20.jpg" alt="">
                      </div>
                    <div class="offer-details">
                      <div class="car-info">
                      <h3>HYUNDAI i20</h3>
                      <p>HYUNDAI i20 Hatchback
                      i20 1.2 Classic +</p>
                      </div>
                      <div class="price">
                        <span class="price-value">662</span>
                        <span class="currency"><span class="bold">pln* </span>/mies.</span>
                      </div>
                      <span class="plus">+</span>
                      <div class="price-km">
                        <span class="price-value">24</span>
                        <span class="currency"><span class="bold">gr</span>/km</span>
                      </div>
                  </div>
                    </div>


                      <div class="offers-loaded" data-loaded-offers-range="1000">

                      <div class="offer-box" data-car-type-code="124606-1">
                      <div class="offer-img-wrapper">
                      <img src="dest/images/golf.jpg" alt="">
                      </div>
                    <div class="offer-details">
                      <div class="car-info">
                      <h3>VW Golf</h3>
                      <p>VOVOLKSWAGEN Polo Hatchback
                      Polo 1.0 Trendline</p>
                      </div>
                      <div class="price">
                        <span class="price-value">780</span>
                        <span class="currency"><span class="bold">pln* </span>/mies.</span>
                      </div>
                      <span class="plus">+</span>
                      <div class="price-km">
                        <span class="price-value">20</span>
                        <span class="currency"><span class="bold">gr</span>/km</span>
                      </div>
                  </div>
                    </div>
                                          <div class="offer-box" data-car-type-code="123842-1">
                                          <div class="offer-img-wrapper">
                                              <img src="dest/images/tipo.jpg" alt="">
                                          </div>
                                            <div class="offer-details">
                                              <div class="car-info">
                                              <h3>FIAT Tipo</h3>
                                              <p>FIAT Tipo Hatchback
                                              Tipo 1.4 16v Pop
                                              </p>
                                              </div>
                                              <div class="price">
                                                <span class="price-value">780</span>
                                                <span class="currency"><span class="bold">pln* </span>/mies.</span>
                                              </div>
                                              <span class="plus">+</span>
                                              <div class="price-km">
                                                <span class="price-value">18</span>
                                                <span class="currency"><span class="bold">gr</span>/km</span>
                                              </div>
                                          </div>
                                            </div>
                                            <div class="offer-box hot-deal" data-car-type-code="128947-1">
                                            <div class="offer-img-wrapper">
                                              <img src="dest/images/megane.jpg" alt="">
                            </div>
                                            <div class="offer-details">
                                              <div class="car-info">
                                              <h3>RENAULT Megane</h3>
                                              <p>RENAULT Megane Hatchback
                                              Megane 1.6 SCe Life</p>
                                              </div>
                                              <div class="price">
                                                <span class="price-value">807</span>
                                                <span class="currency"><span class="bold">pln* </span>/mies.</span>
                                              </div>
                                              <span class="plus">+</span>
                                              <div class="price-km">
                                                <span class="price-value">25</span>
                                                <span class="currency"><span class="bold">gr</span>/km</span>
                                              </div>
                                          </div>
                                            </div>
                                            <div class="offer-box hot-deal" data-car-type-code="119336-1">

                      <div class="offer-img-wrapper">
                      <img src="dest/images/2008.jpg" alt="Skoda Fabia">
                        </div>
                    <div class="offer-details">
                      <div class="car-info">
                      <h3>PEUGEOT 2008</h3>
                      <p>PEUGEOT 2008 Crossover
                      2008 1.2 Pure Tech Active S&S EAT6</p>
                      </div>
                      <div class="price">
                        <span class="price-value">809</span>
                        <span class="currency"><span class="bold">pln* </span>/mies.</span>
                      </div>
                      <span class="plus">+</span>
                        <div class="price-km">
                          <span class="price-value">24</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                  </div>
                    </div>
                    <div class="offer-box hot-deal" data-car-type-code="110908-2">
                    <div class="offer-img-wrapper">
                        <img src="dest/images/auris.jpg" alt="">
                    </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>TOYOTA Auris</h3>
                        <p>TOYOTA Auris Hatchback
                        Auris 1.6 Premium
                        </p>
                        </div>
                        <div class="price">
                          <span class="price-value">833</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                        <div class="price-km">
                          <span class="price-value">23</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>
                      <div class="offer-box" data-car-type-code="109446-2">
                      <div class="offer-img-wrapper">
                        <img src="dest/images/mazda2.jpg" alt="">
      </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>MAZDA 2</h3>
                        <p>MAZDA 2 Hatchback
2 1.5 Skymotion</p>
                        </div>
                        <div class="price">
                          <span class="price-value">776</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                        <div class="price-km">
                          <span class="price-value">20</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>
                      </div>
<button class="load-more" data-range="1000" type="button">Ładuj więcej</button>
                    </div>



                    <div class="offers-wrapper offers-1500" data-offers-range="1500">
                        <div class="offer-box" data-car-type-code="111284-1">
                      <div class="offer-img-wrapper">
                          <img src="dest/images/mondeo.jpg" alt="">
      </div>
                        <div class="offer-details">
                          <div class="car-info">
                          <h3>FORD Mondeo</h3>
                          <p>FORD Mondeo Sedan
                          Mondeo 2.0 TDCi Trend</p>
                          </div>
                          <div class="price">
                            <span class="price-value">1046</span>
                            <span class="currency"><span class="bold">pln* </span>/mies.</span>
                          </div>
                        <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">25</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                      </div>
                        </div>
                        <div class="offer-box hot-deal" data-car-type-code="128601-1">
                      <div class="offer-img-wrapper">
                        <img src="dest/images/karoq.jpg" alt="">
      </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>SKODA Karoq</h3>
                        <p>SKODA Karoq SUV
                        Karoq 1.5 TSI ACT 4x2 Ambition</p>
                        </div>
                        <div class="price">
                          <span class="price-value">1055</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">30</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>
                      <div class="offer-box hot-deal" data-car-type-code="120895-1">
                      <div class="offer-img-wrapper">
                          <img src="dest/images/bmw1.jpg" alt="">
      </div>
                        <div class="offer-details">
                          <div class="car-info">
                          <h3>BMW Seria 1</h3>
                          <p>BMW Seria 1 Hatchback
                          118i aut</p>
                          </div>
                          <div class="price">
                            <span class="price-value">1108</span>
                            <span class="currency"><span class="bold">pln* </span>/mies.</span>
                          </div>
                          <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">39</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                      </div>
                        </div>
                        <div class="offer-box hot-deal" data-car-type-code="124473-2">
                      <div class="offer-img-wrapper">
                          <img src="dest/images/gla.jpg" alt="">
      </div>
                        <div class="offer-details">
                          <div class="car-info">
                          <h3>MERCEDES-BENZ GLA</h3>
                          <p>MERCEDES-BENZ GLA Crossover
                          GLA 200 7G-DCT</p>
                          </div>
                          <div class="price">
                            <span class="price-value">1118</span>
                            <span class="currency"><span class="bold">pln* </span>/mies.</span>
                          </div>
                          <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">39</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                      </div>
                        </div>
                        <div class="offer-box hot-deal" data-car-type-code="126319-2">
                        <div class="offer-img-wrapper">
                        <img src="dest/images/compass.jpg" alt="">
                        </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>JEEP Compass</h3>
                        <p>JEEP Compass SUV
                        Compass 1.4 TMair Longitude FWD S&S</p>
                        </div>
                        <div class="price">
                          <span class="price-value">1131</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                        <div class="price-km">
                          <span class="price-value">33</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>
                      <div class="offer-box hot-deal" data-car-type-code="124640-1">

                      <div class="offer-img-wrapper">
                      <img src="dest/images/countryman.jpg" alt="Skoda Fabia">
                        </div>
                    <div class="offer-details">
                      <div class="car-info">
                      <h3>MINI Countryman</h3>
                      <p>MINI [BMW] Countryman Kombi
                      Cooper S ALL4</p>
                      </div>
                      <div class="price">
                        <span class="price-value">1163</span>
                        <span class="currency"><span class="bold">pln* </span>/mies.</span>
                      </div>
                      <span class="plus">+</span>
                        <div class="price-km">
                          <span class="price-value">36</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                  </div>
                    </div>
                        <div class="offers-loaded" data-loaded-offers-range="1500">

                    <div class="offer-box hot-deal" data-car-type-code="131116-1">
                    <div class="offer-img-wrapper">
                        <img src="dest/images/mazda6.jpg" alt="">
                    </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>MAZDA 6</h3>
                        <p>MAZDA 6 Kombi
                        6 2.0 SkyEnergy
                        </p>
                        </div>
                        <div class="price">
                          <span class="price-value">1222</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                        <div class="price-km">
                          <span class="price-value">29</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>
                      <div class="offer-box hot-deal" data-car-type-code="130886-1">
                      <div class="offer-img-wrapper">
                        <img src="dest/images/klasa-a.jpg" alt="">
      </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>MERCEDES-BENZ A Klasa</h3>
                        <p>MERCEDES-BENZ A Klasa Hatchback
                        A 200 7G-DCT</p>
                        </div>
                        <div class="price">
                          <span class="price-value">1323</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                        <div class="price-km">
                          <span class="price-value">38</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>
                      <div class="offer-box" data-car-type-code="121852-1">
                    <div class="offer-img-wrapper">
                        <img src="dest/images/q2.jpg" alt="">
                    </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>AUDI Q2</h3>
                        <p>AUDI Q2 Crossover
                        Q2 1.4 TFSI CoD
                        </p>
                        </div>
                        <div class="price">
                          <span class="price-value">1236</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                        <div class="price-km">
                          <span class="price-value">31</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>
                      <div class="offer-box" data-car-type-code="125359-1">
                      <div class="offer-img-wrapper">
                          <img src="dest/images/cx-5.jpg" alt="">
                      </div>
                        <div class="offer-details">
                          <div class="car-info">
                          <h3>MAZDA CX-5</h3>
                          <p>MAZDA CX-5 SUV
                          CX-5 2.0 Skymotion 2WD
                          </p>
                          </div>
                          <div class="price">
                            <span class="price-value">1279</span>
                            <span class="currency"><span class="bold">pln* </span>/mies.</span>
                          </div>
                          <span class="plus">+</span>
                          <div class="price-km">
                            <span class="price-value">45</span>
                            <span class="currency"><span class="bold">gr</span>/km</span>
                          </div>
                      </div>
                        </div>
                        <div class="offer-box" data-car-type-code="126971-1">
                      <div class="offer-img-wrapper">
                          <img src="dest/images/a3.jpg" alt="">
                      </div>
                        <div class="offer-details">
                          <div class="car-info">
                          <h3>AUDI A3</h3>
                          <p>AUDI A3 Hatchback
                          A3 1.5 TFSI
                          </p>
                          </div>
                          <div class="price">
                            <span class="price-value">1504</span>
                            <span class="currency"><span class="bold">pln* </span>/mies.</span>
                          </div>
                          <span class="plus">+</span>
                          <div class="price-km">
                            <span class="price-value">36</span>
                            <span class="currency"><span class="bold">gr</span>/km</span>
                          </div>
                      </div>
                        </div>
                        <div class="offer-box hot-deal" data-car-type-code="130855-1">
                      <div class="offer-img-wrapper">
                          <img src="dest/images/xc60.jpg" alt="">
                      </div>
                        <div class="offer-details">
                          <div class="car-info">
                          <h3>VOLVO XC60</h3>
                          <p>VOLVO XC 60 SUV
                          XC 60 T8 AWD Plug-In Hybrid Inscription
                          </p>
                          </div>
                          <div class="price">
                            <span class="price-value">1432</span>
                            <span class="currency"><span class="bold">pln* </span>/mies.</span>
                          </div>
                          <span class="plus">+</span>
                          <div class="price-km">
                            <span class="price-value">38</span>
                            <span class="currency"><span class="bold">gr</span>/km</span>
                          </div>
                      </div>
                        </div>
                      </div>
<button class="load-more" data-range="1500" type="button">Ładuj więcej</button>
                      </div>


                      <div class="offers-wrapper offers-2000" data-offers-range="2000">
                        <div class="offer-box" data-car-type-code="121265-1">
                      <div class="offer-img-wrapper">
                          <img src="dest/images/giulia.jpg" alt="">
      </div>
                        <div class="offer-details">
                          <div class="car-info">
                          <h3>ALFA ROMEO Giulia</h3>
                          <p>ALFA ROMEO Giulia Sedan
                          Giulia 2.0 Turbo aut</p>
                          </div>
                          <div class="price">
                            <span class="price-value">1780</span>
                            <span class="currency"><span class="bold">pln* </span>/mies.</span>
                          </div>
                          <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">52</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                      </div>
                        </div>
                        <div class="offer-box" data-car-type-code="127626-1">
                      <div class="offer-img-wrapper">
                          <img src="dest/images/xtrail.jpg" alt="">
      </div>
                        <div class="offer-details">
                          <div class="car-info">
                          <h3>NISSAN X-Trail</h3>
                          <p>NISSAN X-Trail SUV
                          X-Trail 2.0 dCi Tekna 4WD Xtronicc</p>
                          </div>
                          <div class="price">
                            <span class="price-value">1802</span>
                            <span class="currency"><span class="bold">pln* </span>/mies.</span>
                          </div>
                          <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">64</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                      </div>
                        </div>
                        <div class="offer-box" data-car-type-code="125594-2">
                      <div class="offer-img-wrapper">
                        <img src="dest/images/bmw4.jpg" alt="">
      </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>BMW Seria 4</h3>
                        <p>BMW Seria 4 Coupe
420i Advantage aut</p>
                        </div>
                        <div class="price">
                          <span class="price-value">1871</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">67</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>

                        <div class="offer-box hot-deal" data-car-type-code="118400-1">
                      <div class="offer-img-wrapper">
                          <img src="dest/images/a4.jpg" alt="">
      </div>
                        <div class="offer-details">
                          <div class="car-info">
                          <h3>AUDI A4 Kombi</h3>
                          <p>AUDI A4 Kombi
                          A4 2.0 TDI S tronic</p>
                          </div>
                          <div class="price">
                            <span class="price-value">1955</span>
                            <span class="currency"><span class="bold">pln* </span>/mies.</span>
                          </div>
                          <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">58</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                      </div>
                        </div>
                        <div class="offer-box" data-car-type-code="119195-1">
                        <div class="offer-img-wrapper">
                        <img src="dest/images/cla.jpg" alt="">
                        </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>MERCEDES-BENZ CLA</h3>
                        <p>MERCEDES-BENZ CLA Coupe
                        AMG CLA 45 4-Matic</p>
                        </div>
                        <div class="price">
                          <span class="price-value">1762</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                        <div class="price-km">
                          <span class="price-value">52</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>
                      <div class="offer-box" data-car-type-code="126297-1">
                        <div class="offer-img-wrapper">
                        <img src="dest/images/5008.jpg" alt="">
                        </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>PEUGEOT 5008</h3>
                        <p>PEUGEOT 5008 Crossover
                        5008 2.0 BlueHDI GT S&S EAT8</p>
                        </div>
                        <div class="price">
                          <span class="price-value">1763</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                        <div class="price-km">
                          <span class="price-value">43</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>
                        <div class="offers-loaded" data-loaded-offers-range="2000">
                      <div class="offer-box" data-car-type-code="117955-1">
                        <div class="offer-img-wrapper">
                        <img src="dest/images/s90.jpg" alt="">
                        </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>VOLVO S90</h3>
                        <p>VOLVO S90 Sedan
                        S90 T8 AWD Plug-In Hybrid Inscription</p>
                        </div>
                        <div class="price">
                          <span class="price-value">1845</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                        <div class="price-km">
                          <span class="price-value">44</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>
                      <div class="offer-box hot-deal" data-car-type-code="127871-1">

                      <div class="offer-img-wrapper">
                      <img src="dest/images/stinger.jpg" alt="Skoda Fabia">
                        </div>
                    <div class="offer-details">
                      <div class="car-info">
                      <h3>KIA Stinger</h3>
                      <p>KIA Stinger Sedan
                      Stinger 2.0 T-GDI XL</p>
                      </div>
                      <div class="price">
                        <span class="price-value">1849</span>
                        <span class="currency"><span class="bold">pln* </span>/mies.</span>
                      </div>
                      <span class="plus">+</span>
                        <div class="price-km">
                          <span class="price-value">45</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                  </div>
                    </div>
                    <div class="offer-box hot-deal" data-car-type-code="124135-1">
                    <div class="offer-img-wrapper">
                        <img src="dest/images/navara.jpg" alt="">
                    </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>NISSAN Navara</h3>
                        <p>NISSAN Navara Pickup
                        Navara 2.3 dCi Tekna aut EU6
                        </p>
                        </div>
                        <div class="price">
                          <span class="price-value">1861</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                        <div class="price-km">
                          <span class="price-value">45</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>
                      <div class="offer-box hot-deal" data-car-type-code="120032-1">
                      <div class="offer-img-wrapper">
                        <img src="dest/images/glc.jpg" alt="">
      </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>MERCEDES-BENZ GLC</h3>
                        <p>MERCEDES-BENZ Klasa GLC Crossover
                        GLC Coupe 220 d 4-Matic</p>
                        </div>
                        <div class="price">
                          <span class="price-value">1986</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                        <div class="price-km">
                          <span class="price-value">47</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>
                      <div class="offer-box" data-car-type-code="128358-1">
                      <div class="offer-img-wrapper">
                        <img src="dest/images/stelvio.jpg" alt="">
      </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>ALFA ROMEO Stelvio</h3>
                        <p>ALFA ROMEO Stelvio SUV
                        Stelvio 2.0 Turbo Executive Q4 aut</p>
                        </div>
                        <div class="price">
                          <span class="price-value">1730</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                        <div class="price-km">
                          <span class="price-value">50</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>
                      <div class="offer-box" data-car-type-code="128718-1">
                      <div class="offer-img-wrapper">
                        <img src="dest/images/q50.jpg" alt="">
      </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>INFINITI Q50</h3>
                        <p>INFINITI Q50 Sedan
                        Q50 2.0t Premium aut</p>
                        </div>
                        <div class="price">
                          <span class="price-value">1722</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                        <div class="price-km">
                          <span class="price-value">41</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>
                      </div>
<button class="load-more" data-range="2000" type="button">Ładuj więcej</button>

                      </div>


                      <div class="offers-wrapper offers-2500" data-offers-range="2500">
                    <div class="offer-box" data-car-type-code="123783-1">
                    <div class="offer-img-wrapper">
                          <img src="dest/images/cclass.jpg" alt="">
      </div>
                        <div class="offer-details">
                          <div class="car-info">
                          <h3>MERCEDES-BENZ C Klasa</h3>
                          <p>MERCEDES-BENZ C Klasa Kombi
                          C 200 9G-TRONIC</p>
                          </div>
                          <div class="price">
                            <span class="price-value">2055</span>
                            <span class="currency"><span class="bold">pln* </span>/mies.</span>
                          </div>
                          <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">75</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                      </div>
                        </div>
                        <div class="offer-box" data-car-type-code="112636-1">
                      <div class="offer-img-wrapper">
                      <img src="dest/images/xe.jpg" alt="">
      </div>
                    <div class="offer-details">
                      <div class="car-info">
                      <h3>JAGUAR XE</h3>
                      <p>JAGUAR XE Sedan
                      XE 2.0 D Prestige aut</p>
                      </div>
                      <div class="price">
                        <span class="price-value">2067</span>
                        <span class="currency"><span class="bold">pln* </span>/mies.</span>
                      </div>
                      <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">60</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                  </div>
                    </div>
                        <div class="offer-box hot-deal" data-car-type-code="131016-1">
                      <div class="offer-img-wrapper">
                        <img src="dest/images/x3.jpg" alt="">
      </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>BMW X3</h3>
                        <p>BMW X3 SUV
X3 xDrive20d Advantage aut</p>
                        </div>
                        <div class="price">
                          <span class="price-value">2109</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">75</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>

                      <div class="offer-box hot-deal" data-car-type-code="112303-1">
                      <div class="offer-img-wrapper">
                          <img src="dest/images/superb.jpg" alt="">
      </div>
                        <div class="offer-details">
                          <div class="car-info">
                          <h3>SKODA Superb</h3>
                          <p>SKODA Superb Sedan
                          Superb 2.0 TDI 4x4 L&K DSG</p>
                          </div>
                          <div class="price">
                            <span class="price-value">1734</span>
                            <span class="currency"><span class="bold">pln* </span>/mies.</span>
                          </div>
                          <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">49</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                      </div>
                        </div>

                        <div class="offer-box" data-car-type-code="117803-1">
                      <div class="offer-img-wrapper">
                          <img src="dest/images/f-pace.jpg" alt="">
      </div>
                        <div class="offer-details">
                          <div class="car-info">
                          <h3>JAGUAR F-PACE</h3>
                          <p>JAGUAR F-Pace SUV
                          F-Pace 2.0 i4D AWD Prestige</p>
                          </div>
                          <div class="price">
                            <span class="price-value">2103</span>
                            <span class="currency"><span class="bold">pln* </span>/mies.</span>
                          </div>
                          <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">51</span></span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                      </div>
                        </div>

                        <div class="offer-box" data-car-type-code="122710-1">
                      <div class="offer-img-wrapper">
                          <img src="dest/images/klasa-e.jpg" alt="">
      </div>
                        <div class="offer-details">
                          <div class="car-info">
                          <h3>MERCEDES E Klasa</h3>
                          <p>MERCEDES-BENZ E Klasa Sedan
                          E 220 d 4-Matic 9G-TRONIC</p>
                          </div>
                          <div class="price">
                            <span class="price-value">2105</span>
                            <span class="currency"><span class="bold">pln* </span>/mies.</span>
                          </div>
                          <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">51</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                      </div>
                        </div>
                        <div class="offers-loaded" data-loaded-offers-range="2500">
                      <div class="offer-box" data-car-type-code="118524-1">
                        <div class="offer-img-wrapper">
                        <img src="dest/images/v90.jpg" alt="">
                        </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>VOLVO V90</h3>
                        <p>VOLVO V90 Kombi
V90 D4 Momentum</p>
                        </div>
                        <div class="price">
                          <span class="price-value">2186</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                        <div class="price-km">
                          <span class="price-value">54</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>
                      <div class="offer-box hot-deal" data-car-type-code="118604-1">

                      <div class="offer-img-wrapper">
                      <img src="dest/images/xc90.jpg" alt="Skoda Fabia">
                        </div>
                    <div class="offer-details">
                      <div class="car-info">
                      <h3>VOLVO XC90</h3>
                      <p>VOLVO XC 90 SUV
XC 90 T8 AWD Plug-In Hybrid R-Design 7os</p>
                      </div>
                      <div class="price">
                        <span class="price-value">2245</span>
                        <span class="currency"><span class="bold">pln* </span>/mies.</span>
                      </div>
                      <span class="plus">+</span>
                        <div class="price-km">
                          <span class="price-value">53</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                  </div>
                    </div>
                    <div class="offer-box" data-car-type-code="124018-2">
                    <div class="offer-img-wrapper">
                        <img src="dest/images/q5.jpg" alt="">
                    </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>AUDI Q5</h3>
                        <p>AUDI Q5 SUV
                        Q5 2.0 TFSI Quattro S tronic
                        </p>
                        </div>
                        <div class="price">
                          <span class="price-value">2315</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                        <div class="price-km">
                          <span class="price-value">58</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>
                      <div class="offer-box" data-car-type-code="125609-1">
                      <div class="offer-img-wrapper">
                        <img src="dest/images/XF.jpg" alt="">
      </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>JAGUAR XF</h3>
                        <p>JAGUAR XF Sedan
                        XF 2.0 i4D AWD Portfolio aut</p>
                        </div>
                        <div class="price">
                          <span class="price-value">2364</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                        <div class="price-km">
                          <span class="price-value">60</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>
                      <!--
                      <div class="offer-box" data-car-type-code="126846-1">
                      <div class="offer-img-wrapper">
                        <img src="dest/images/bmw-5-kb.jpg" alt="">
      </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>BMW Seria 5 Kombi
                        530d xDrive aut
                        </p>
                        </div>
                        <div class="price">
                          <span class="price-value">2010</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                        <div class="price-km">
                          <span class="price-value">60</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>
                      <div class="offer-box" data-car-type-code="126846-1">
                      <div class="offer-img-wrapper">
                        <img src="dest/images/gl.jpg" alt="">
      </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>MERCEDES-BENZ GL</h3>
                        <p>MERCEDES-BENZ Klasa GL SUV
                        GLS 400 4-Matic</p>
                        </div>
                        <div class="price">
                          <span class="price-value">2494</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                        <div class="price-km">
                          <span class="price-value">59</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>
                      -->
                      </div>
<button class="load-more" data-range="2500" type="button">Ładuj więcej</button>
                      </div>


                      <div class="offers-wrapper offers-max" data-offers-range="max">
                        <div class="offer-box" data-car-type-code="118558-1">
                      <div class="offer-img-wrapper">
                          <img src="dest/images/macan.jpg" alt="">
      </div>
                        <div class="offer-details">
                          <div class="car-info">
                          <h3>PORSCHE Macan</h3>
                          <p>Crossover
                          Macan
                          252KM, 2L, 4 cylindry, Automatyczna, Stały napęd na 4 koła off road</p>
                          </div>
                          <div class="price">
                            <span class="price-value">3014</span>
                            <span class="currency"><span class="bold">pln* </span>/mies.</span>
                          </div>
                          <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">91</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                      </div>
                        </div>
                        <div class="offer-box" data-car-type-code="125119-1">
                      <div class="offer-img-wrapper">
                        <img src="dest/images/cherokee.jpg" alt="">
      </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>JEEP Grand Cherokee</h3>
                        <p>JEEP Grand Cherokee SUV
Gr. Cherokee 3.0 CRD Overland</p>
                        </div>
                        <div class="price">
                          <span class="price-value">2704</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">86</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>
                      <div class="offer-box" data-car-type-code="122970-1">
                      <div class="offer-img-wrapper">
                          <img src="dest/images/discovery.jpg" alt="">
      </div>
                        <div class="offer-details">
                          <div class="car-info">
                          <h3>Range Rover Discovery</h3>
                          <p>LAND ROVER Discovery SUV
                          Discovery V 2.0 SD4 SE</p>
                          </div>
                          <div class="price">
                            <span class="price-value">2823</span>
                            <span class="currency"><span class="bold">pln* </span>/mies.</span>
                          </div>
                          <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">86</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                      </div>
                        </div>
                        <div class="offer-box hot-deal" data-car-type-code="123648-1">
                        <div class="offer-img-wrapper">
                            <img src="dest/images/bmw-5.jpg" alt="">
        </div>
                          <div class="offer-details">
                            <div class="car-info">
                            <h3>BMW Seria 5 Sedan</h3>
                            <p>BMW Seria 5 Sedan
                            520d xDrive aut</p>
                            </div>
                            <div class="price">
                              <span class="price-value">2581</span>
                              <span class="currency"><span class="bold">pln* </span>/mies.</span>
                            </div>
                            <span class="plus">+</span>
                            <div class="price-km">
                            <span class="price-value">81</span>
                            <span class="currency"><span class="bold">gr</span>/km</span>
                          </div>
                        </div>
                          </div>
                          <div class="offer-box" data-car-type-code="124153-1">
                      <div class="offer-img-wrapper">
                          <img src="dest/images/klasa-e-c.jpg" alt="">
      </div>
                        <div class="offer-details">
                          <div class="car-info">
                          <h3>MERCEDES-BENZ E Klasa Coupe</h3>
                          <p>MERCEDES-BENZ E Klasa Coupe
                          E 300 Coupe 9G-TRONIC</p>
                          </div>
                          <div class="price">
                            <span class="price-value">3104</span>
                            <span class="currency"><span class="bold">pln* </span>/mies.</span>
                          </div>
                          <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">79</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                      </div>
                        </div>
                        <div class="offer-box" data-car-type-code="115289-1">
                      <div class="offer-img-wrapper">
                          <img src="dest/images/rx.jpg" alt="">
      </div>
                        <div class="offer-details">
                          <div class="car-info">
                          <h3>LEXUS RX</h3>
                          <p>LEXUS RX SUV
                          RX 450h Elegance</p>
                          </div>
                          <div class="price">
                            <span class="price-value">2686</span>
                            <span class="currency"><span class="bold">pln* </span>/mies.</span>
                          </div>
                          <span class="plus">+</span>
                          <div class="price-km">
                          <span class="price-value">70</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                      </div>
                        </div>
                        <div class="offers-loaded" data-loaded-offers-range="max">
                      <div class="offer-box" data-car-type-code="130310-1">
                        <div class="offer-img-wrapper">
                        <img src="dest/images/klasa-g.jpg" alt="">
                        </div>
                      <div class="offer-details">
                        <div class="car-info">
                        <h3>MERCEDES-BENZ Klasa G</h3>
                        <p>MERCEDES-BENZ Klasa G SUV
                        G 500</p>
                        </div>
                        <div class="price">
                          <span class="price-value">4190</span>
                          <span class="currency"><span class="bold">pln* </span>/mies.</span>
                        </div>
                        <span class="plus">+</span>
                        <div class="price-km">
                          <span class="price-value">144</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                    </div>
                      </div>
                      <div class="offer-box" data-car-type-code="114964-1">

                      <div class="offer-img-wrapper">
                      <img src="dest/images/bmw-7.jpg" alt="Skoda Fabia">
                        </div>
                    <div class="offer-details">
                      <div class="car-info">
                      <h3>BMW Seria 7</h3>
                      <p>BMW Seria 7 Sedan
                      730Ld xDrive</p>
                      </div>
                      <div class="price">
                        <span class="price-value">4214</span>
                        <span class="currency"><span class="bold">pln* </span>/mies.</span>
                      </div>
                      <span class="plus">+</span>
                        <div class="price-km">
                          <span class="price-value">108</span>
                          <span class="currency"><span class="bold">gr</span>/km</span>
                        </div>
                  </div>
                    </div>

                      </div>
<button class="load-more" data-range="max" type="button">Ładuj więcej</button>
                      </div>
                  </div>
                  <div class="cell large-12 speciallarge-4 padding-less speciallarge relative" style="position: relative;">
                      <img src="dest/images/shape-form.svg" class="bg-image" alt="">

                     </div>
                     <div class="Box natural">
                    <div class="showbox" style="display: none">
                            <div class="loader"></div>
                            <p class="loading_info">Trwa przesyłanie formularza</p>
                        </div>
                        <div class="success" style="display: none">
                            <img src="<?= $tpl ?>/dest/images/tickYES.png" alt="" class="successForm">
                            <p class="loading_info successInfo"><h3>Dziękujemy.</h3> <br/>Skontaktujemy się z Tobą wkrótce.</p>
                        </div>
                           <div class="Form">
                              <p class="headtext">Wypełnij formularz</p>
                              <p class="undertext">i dowiedz się, jak tani może być nowy samochód!</p>
                              <div class="Form-wrapper">
                                 <div class="Form__input-wrapper">
                                    <input autocomplete="off" type="text" name="client_name" class="Form__input client_name">
                                    <label class="Form__input-label client_name-label">Imię</label>
                                    <img src="<?= $tpl ?>/dest/images/tickYES.svg" alt="" class="inputValid">
                                 </div>
                                 <div class="Form__input-wrapper">
                                          <input autocomplete="off" type="text" name="client_email" class="Form__input client_email">
                                          <label class="Form__input-label client_email-label">E-mail</label>
                                          <img src="<?= $tpl ?>/dest/images/tickYES.svg" alt="" class="inputValid">
                                       </div>
                                       <div class="Form__input-wrapper">
                                          <input autocomplete="off" type="text" name="client_mobile" class="Form__input client_mobile">
                                          <label class="Form__input-label client_mobile-label">Telefon</label>
                                          <img src="<?= $tpl ?>/dest/images/tickYES.svg" alt="" class="inputValid">
                                       </div>
                                       <div class="Form__input-wrapper">
                                          <select name="client_choice" class="client_choice">
                                          <option disabled selected value>Wybierz samochód</option>
                                          <option value="121265-1">ALFA ROMEO Giulia</option>
                                          <option value="128358-1">ALFA ROMEO Stelvio</option>
                                          <option value="126971-1">AUDI A3</option>
                                          <option value="118400-1">AUDI A4 Kombi</option>
                                          <option value="130563-1">AUDI A7 Sedan</option>
                                          <option value="121852-1">AUDI Q2</option>
                                          <option value="124018-2">AUDI Q5</option>
                                          <option value="120895-1">BMW Seria 1</option>
                                          <option value="114603-1">BMW Seria 3</option>
                                          <option value="125594-2">BMW Seria 4</option>
                                          <option value="123648-1">BMW Seria 5 Sedan</option>
                                          <option value="126846-1">BMW Seria 5 Kombii</option>
                                          <option value="114964-1">BMW Seria 7</option>
                                          <option value="131016-1">BMW X3</option>
                                          <option value="128051-1">CITROEN C3 Aircross</option>
                                          <option value="123842-1">FIAT Tipo</option>
                                          <option value="130696-2">FORD Focus</option>
                                          <option value="111284-1">FORD Mondeo</option>
                                          <option value="109899-1">Hyundai i20</option>
                                          <option value="128718-1">Infinity Q50</option>
                                          <option value="117803-1">JAGUAR F-Pace</option>
                                          <option value="112636-1">JAGUAR XE</option>
                                          <option value="125609-1">JAGUAR XF</option>
                                          <option value="126319-2">JEEP Compass</option>
                                          <option value="125119-1">JEEP Grand Cherokee</option>
                                          <option value="127871-1">KIA Stinger</option>
                                          <option value="122970-1">LAND ROVER Discovery</option>
                                          <option value="109446-2">MAZDA 2</option>
                                          <option value="131116-1">MAZDA 6</option>
                                          <option value="125359-1">MAZDA CX-5</option>
                                          <option value="130886-1">MERCEDES-BENZ A Klasa</option>
                                          <option value="123783-1">MERCEDES-BENZ C Klasa</option>
                                          <option value="119195-1">MERCEDES-BENZ CLA</option>
                                          <option value="122710-1">MERCEDES-BENZ E Klasa Sedan</option>
                                          <option value="124153-1">MERCEDES-BENZ E Klasa Coupe</option>
                                          <option value="124473-2">MERCEDES-BENZ GLA</option>
                                          <option value="120032-1">MERCEDES-BENZ GLC</option>
                                          <option value="130310-1">MERCEDES-BENZ Klasa G</option>
                                          <option value="124640-1">MINI Countryman</option>
                                          <option value="124135-1">NISSAN Navara</option>
                                          <option value="126846-1">NISSAN Qashqai</option>
                                          <option value="127626-1">NISSAN X-Trail</option>
                                          <option value="115289-1">LEXUS RX</option>
                                          <option value="108839-1">OPEL Corsa</option>
                                          <option value="124991-1">OPEL Insignia</option>
                                          <option value="122551-1">OPEL Mokka</option>
                                          <option value="119336-1">PEUGEOT 2008</option>
                                          <option value="126297-1">PEUGEOT 5008</option>
                                          <option value="118558-1">PORSCHE Macan</option>
                                          <option value="128947-1">RENAULT Megane</option>
                                          <option value="126654-1">SKODA Fabia</option>
                                          <option value="128601-1">SKODA Karoq</option>
                                          <option value="112303-1">SKODA Superb</option>
                                          <option value="110908-2">TOYOTA Auris</option>
                                          <option value="124606-1">VOLKSWAGEN Golf</option>
                                          <option value="109196-1">VOLKSWAGEN Passat</option>
                                          <option value="117955-1">VOLVO S90</option>
                                          <option value="118524-1">VOLVO V90</option>
                                          <option value="130855-1">VOLVO XC 60</option>
                                          <option value="118604-1">VOLVO XC 90</option>

                                          </select>
                                          <img src="<?= $tpl ?>/dest/images/tickYES.svg" alt="" class="inputValid">
                                       </div>

                                 <div class="grid-x grid-margin-x check-company-normalize">
                                    <div class="cell xphone-auto Agrees terms-align agreeAndButton">
                                       <div class="Form__input-wrapper terms company-check Agrees">
                                          <!-- <label for="checkNIP" class="checkNIP-label">
                                             <span class="checkbox checkbox-large"><span class="checked"></span></span> -->


                                             <!-- <input type="checkbox" hidden="hidden" id="username" value="test"> -->

                                          <input type="checkbox" id="checkNIP" name="checkNIP" value="true">


                                             <label class="switch" for="checkNIP" class="checkNIP-label"></label>


                                             <span class="terms-text company-terms-text nip-padd">Prowadzisz firmę?</span>
                                             <div class="error formFirst"></div>
                                          </label>
                                       </div>
                                    </div>
                                    <div class="cell xphone-auto lessLeft nip-normalize">
                                       <div class="Form__input-wrapper">
                                          <input autocomplete="off" type="text" name="client_nip" class="Form__input client_nip" id="client_nip">
                                          <label class="Form__input-label client_nip_label hide-label">NIP</label>
                                          <img src="<?= $tpl ?>/dest/images/tickYES.svg" alt="" class="inputValid">
                                       </div>
                                    </div>
                                 </div>
                                 <div class="agreeAndButton">
                                    <div class="Agrees">
                                       <div class="cell auto">
                                          <div class="terms check-all-wrapper">
                                             <div class="company-check">
                                                <!-- <input type="checkbox" id="checkAll" name="zaznaczAll" value="true">
                                                <label for="checkAll"> -->


                                                <input type="checkbox" id="checkAll" name="checkAll" value="true">
                                             <label class="switch switch-agrees" for="checkAll" class="checkNIP-label"></label>


<!-- <span class="checkbox checkbox-large"><span class="checked"></span></span> -->

                                                   <span class="terms-text"><b>Zaznacz wszystkie zgody.</b></span>
                                                   <div class="error formFirst"></div>
                                                </label>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <button type="submit" class="Form__SendBtn">
                                    Wyślij
                                    </button>
                                 </div>
                              </div>
                           </div>
      <!-- </form> -->
                        </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="Agrees Agrees__container BoxWithShadow BoxShadowAgrees">
            <div class="formSendContainer" style="display: none"></div>
            <div class="row">
               <div class="grid-x grid-padding-x">
                  <div class="cell xphone-4">
                     <div class="terms">
                        <input type="checkbox" agree="general" id="form_agree1" name="form_agree1" value="true" <?= $agreements[6]['required'] ? 'required' : '' ?>>
                        <label for="form_agree1">
                           <span class="checkbox"><span class="checked"></span></span>
                           <span class="terms-text"><?= $agreements[6]['content'] ?></span>
                           <div class="error errorPlacement"></div>
                        </label>
                     </div>
                     <div class="terms">
                        <input type="checkbox" agree="general" id="form_agree2" name="form_agree2" value="true" <?= $agreements[7]['required'] ? 'required' : '' ?>>
                        <label for="form_agree2">
                           <span class="checkbox"><span class="checked"></span></span>
                           <span class="terms-text"><?= $agreements[7]['content'] ?></span>
                           <p class="error errorPlacement"></p>
                        </label>
                     </div>
                  </div>
                  <div class="cell xphone-4">
                     <div class="terms">
                        <input type="checkbox" agree="general" id="form_agree3" name="form_agree3" value="true" <?= $agreements[8]['required'] ? 'required' : '' ?>>
                        <label for="form_agree3">
                           <span class="checkbox"><span class="checked"></span></span>
                           <span class="terms-text"><?= $agreements[8]['content'] ?></span>
                           <p class="error errorPlacement"></p>
                        </label>
                     </div>
                  </div>
                  <div class="cell xphone-4">
                     <div class="terms">
                        <input type="hidden" name="form_agree4" value="false">
                        <input type="checkbox" agree="general" id="form_agree4" name="form_agree4" value="true" <?= $agreements[9]['required'] ? 'required' : '' ?>>
                        <label for="form_agree4">
                           <span class="checkbox"><span class="checked"></span></span>
                           <span class="terms-text"><?= $agreements[9]['content'] ?></span>
                           <p class="error errorPlacement"></p>
                        </label>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="Rodo_disclaimer">
                  <div class="grid-x grid-padding-x">
                     <div class="cell auto rodo_text">
                        <?= $agreements[10]['content'] ?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <footer class="Footer">
            <div class="row">
               <div class="grid-x grid-padding-x">
                  <div class="cell">
                     <div class="menu-block">
                        <p class="title">Carsmile <span class="copyright">© 2018</span></p>
                        <p class="Footer-disclaimer">
                           Carsmile Spółka Akcyjna z siedzibą w Warszawie, ul. Prosta 51, 00-838 Warszawa, wpisana do rejestru przedsiębiorców prowadzonego przez Sąd Rejonowy dla m.st. Warszawy w Warszawie, XII Wydział Gospodarczy Krajowego Rejestru Sądowego, pod numerem KRS 0000724919, REGON 369367192, NIP 7010800013.
                        </p>
                        <p class="Footer-disclaimer">
                        Złożenie przez nas ostatecznej oferty zależy od Twojej zdolności do płacenia abonamentu obliczonej m.in. w oparciu o Twoje zarobki, zobowiązania oraz historię finansową.
                        </p>
                        <p class="Footer-disclaimer">
                        *Ostateczna wysokość czynszu może się różnić w zależności od wybranego modelu samochodu, uzgodnionego wkładu własnego oraz okresu najmu.
                        </p>
                     </div>
                  </div>
               </div>
            </div>
         </footer>
      </form>

      <!-- All your html code -->
      <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
      <script src="<?= $tpl ?>/dest/js/validate.js"></script>
      <script src="<?= $tpl ?>/dest/js/main.js"></script>
   </body>
</html>
