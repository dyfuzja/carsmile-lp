$(document).ready(function() {

            $('#checkAll').click(function () {

                if ( $(this).prop('checked') === true ) {
                    $('.terms input[agree=general]').prop('checked', true);
                } else {
                    $('.terms input[agree=general]').prop('checked', false);
                }
                validAgree();

            })

            $('.Form__input').on('input', function() {

                $(this).val() ? $(this).next('.Form__input-label').addClass('hasValue') : $(this).next('.Form__input-label').removeClass('hasValue');

            })

            $('.Form__input.error').on('keyup', function(e) {
                e.stopPropagation();
                $(this).before().remove()
            })


            function removeClasses(){
                $('.client_nip').removeClass('valid');
                $('.client_nip.valid').removeClass('valid');
            }

            function validNip(){
                const validator = $( "#LPform" ).validate();
                validator.element( $('.client_nip') );
            }

            function validAgree(){
                var validator = $( "#LPform" ).validate();
                const allAgrees = document.querySelectorAll('.terms input[agree=general]');
                allAgrees.forEach(function(singleAgree){
                validator.element(singleAgree);
                })
            }

            $('.Form__input').on('blur', function(e) {


                if (this === document.querySelectorAll('.client_nip')[0] ) {
                    e.stopPropagation();
                    validNip();

                    if(document.querySelectorAll('.client_nip')[0].value.length === 0) {
                        removeClasses();
                        return
                    } else if(document.querySelectorAll('.client_nip')[0].value.length > 0) {
                        // debugger;
                        removeClasses();
                        validNip();
                        return
                    }
                    return
                }

                var validator = $( "#LPform" ).validate();
                validator.element( this );



            });

            $.validator.addMethod("emailCheck", function( value, element ) {
                return this.optional( element ) || /^.+@.+\.[A-Za-z]{2,}$/.test( value );
                }, "Niepoprawny adres e-mail");

            $.validator.addMethod("lettersonly", function(value, element) {
                return this.optional(element) || /^[a-zżźćńółęąśŻŹĆĄŚĘŁÓŃ\s]+$/i.test(value);
                }, "Proszę wpisać tylko litery");

            $.validator.addMethod("digitsonly", function(value, element) {
                return this.optional(element) || /^[0-9]+$/i.test(value);
                }, "Proszę wpisać tylko cyfry");

            $.validator.addMethod("phonePL", function(phonenr, element) {
                phonenr = phonenr.replace(/\s+/g, "");
                return this.optional(element) || phonenr.length > 8 && phonenr.length < 10 &&
                    phonenr.match(/^(0-?)?(\([0-9]\d{2}\)|[0-9]\d{2})-?[0-9]\d{2}-?\d{3,4}$/);

                }, "Niepoprawny numer telefonu");

                $.validator.addMethod("nip", function(value, element) {
                            var verificator_nip = new Array(6,5,7,2,3,4,5,6,7); var nip = value.replace(/[\ \-]/gi, '');
                            if (nip.length == 0) { return true;}
                            else if (nip.length != 10)  { return false; } else  {
                                var n = 0;
                                for (var i=0; i<9; i++) {	n += nip[i] * verificator_nip[i]; }
                                n %= 11;
                                if (n != nip[9]) { return false; }
                                }
                            return true;
                        }, "Proszę o podanie prawidłowego numeru NIP");

                if ( $('#LPform').length ) {

                    var $form = $('#LPform');

                    $form.validate({
                        rules: {
                            client_email: {
                                required: true,
                                emailCheck: true
                            },
                            client_mobile: {
                                required: true,
                                phonePL: true
                            },
                            client_name: {
                                required: true,
                                lettersonly: true
                            },
                            client_nip: {
                            //   required: true,
                              nip: true
                            },
                            form_agree1: "required",
                            form_agree2: "required",
                            form_agree3: "required"
                        },
                        messages: {
                            client_email: {
                                required: "Pole wymagane"
                            },
                            client_mobile: {
                                required: "Pole wymagane",
                                phonePL: "Niepoprawny nr telefonu"
                            },
                            client_name: {
                                required: "Pole wymagane",
                            },
                            client_nip: {
                              nip: "Podaj poprawny NIP"
                            },
                            form_agree1: "Pole wymagane",
                            form_agree2: "Pole wymagane",
                            form_agree3: "Pole wymagane"

                        },
                        errorPlacement: function(error, element) {

                            var errorCheckboxPlace = $(element).next().find('.errorPlacement');

                            if ( $(element).parent().hasClass('terms') ) {

                                error.appendTo( errorCheckboxPlace );

                            } else {
                                error.insertBefore(element);
                            }

                        },
                        submitHandler: function(form) {
                          $('.Form').css('display', 'none');
                            var $showbox = $('.showbox');
                            var $successSent = $('.success');
                            var $formContainerAgrees = $('.formSendContainer');
                            $showbox.show();
                            $formContainerAgrees.show();


                            $.post( $(form).attr('action'), $(form).serialize(), function (data) {

                                // {"saved":1,"id_lead_www":666,"error":null} = data;
                                var dataResponse = null;

                                if (typeof data === "string") {
                                    dataResponse = JSON.parse(data);
                                } else {
                                    dataResponse = data;
                                }

                                if( dataResponse.saved ){
                                    // $showbox.hide();
                                    // $successSent.show();

                                    if(window['dataLayer']) dataLayer.push({
                                        'event':'STevent',
                                        'eventCategory':'cmLead',
                                        'eventAction':'Form button',
                                        'eventLabel':'Wyslij',
                                        'leadcm': dataResponse.id_lead_www
                                    });
                                    setTimeout(()=>{
                                        window.location="http://lp.carsmile.pl/thank-you"
                                    },4000)

                                    if($('checkNIP').value!='') {
                                        localStorage.setItem('offerType', 'dla-firmy');
                                    } else {
                                        localStorage.setItem('offerType', 'dla-mnie');
                                    }
                                }
                            } );

                            return false;

                            // console.log('Wysłano formularz');
                        },
                        invalidHandler: function(form) {

                            setTimeout(function() {
                                var firstError = $('input.error');
                                var firstErrPosition = firstError.offset().top

                                $("html, body").animate({ scrollTop: firstErrPosition-50 }, 500);
                            }, 300)

                            // console.log('Wystapil blad przy walidacji');
                        }
                    }).settings.ignore = "input[type=text]:hidden";

                }
                $('input').prop('checked', false)

                $('#checkNIP').click(function(){
                  $('.client_nip').val("");
                  validNip();
                 $('.client_nip').blur();
                removeClasses();
                  $('.client_nip').addClass('invalid');
                  $('.client_nip').toggleClass('show-input');
                  $('.client_nip_label').toggleClass('hide-label');
                  $('.client_nip_label').toggleClass('show-label');

                })



        $('.client_nip').on('keyup', function(e){
            if(!$('.client_nip').val().length) {
                e.stopPropagation();
                validNip();
                removeClasses();
            }
        })


        $('form').submit(function(){
            if($('.terms input[agree=general]').hasClass('error')) {
                $('.switch-agrees').addClass('switch-error');
            }
        })


        const allBtn = document.querySelectorAll('.btn-choice');
        const allOfferRange = document.querySelectorAll('.offers-wrapper');


        const handleClick = (e) => {
            e.preventDefault();
            $stick = $('.Box');
            $foot = $('.Agrees__container');
            document.querySelectorAll('.load-more').forEach(node => {
                node.classList.add('btn-visible');
                node.classList.remove('btn-invisible');
            })
            document.querySelectorAll('.offers-loaded').forEach(node => {
                node.classList.remove('offer-loaded-visible');
            })
            margin = 20;
            offtop = $stick.offset().top - margin;
            offbtm = $foot.offset().top - ( margin*2 + $stick.height() );
            test = 0;
            allOfferRange.forEach(node=>{
                node.classList.remove('offer-range-display');
            })

          allBtn.forEach(node => {
            node.classList.remove('active-button');
          });
          $(e.target).addClass("active-button");


          let offerRange = e.target.attributes.getNamedItem('data-range').value;
          let displayCurrent = document.querySelector('[data-offers-range="'+offerRange+'"]');
          displayCurrent.classList.add('offer-range-display');
        }

        allBtn.forEach(node => {
          node.addEventListener('click', handleClick)
        });



        const allOffers = document.querySelectorAll('.offer-box');

        const handleClickOffer = (e) => {


            allOffers.forEach(node => {
              node.classList.remove('offer-active');
            });
            e.currentTarget.classList.add('offer-active');
            e.stopPropagation();
          let offerChoice = e.currentTarget.attributes.getNamedItem('data-car-type-code').value;
          $('.client_choice').val(offerChoice).trigger('change');
          }

        allOffers.forEach(node => {
            node.addEventListener('click', handleClickOffer)
          });


          document.querySelector('.client_choice').addEventListener('change', function(){

            allOffers.forEach(node => {
                node.classList.remove('offer-active');
              });
              let carCode = document.querySelector('.client_choice').value;

              let ChoicedCurrent = document.querySelector('[data-car-type-code="'+carCode+'"]');
              ChoicedCurrent.classList.add('offer-active');
        })







        const allLoadBtn = document.querySelectorAll('.load-more');


        const showMore = (e) => {
            e.preventDefault();
            $(e.target).removeClass('btn-visible');
            $(e.target).addClass('btn-invisible');

          let offerRange = e.target.attributes.getNamedItem('data-range').value;

          let displayCurrentss = document.querySelector('[data-loaded-offers-range="'+offerRange+'"]');
          displayCurrentss.classList.add('offer-loaded-visible');

          $stick = $('.Box');
          $foot = $('.Agrees__container');
          margin = 20;
          offtop = $stick.offset().top - margin;
          offbtm = $foot.offset().top - ( margin*2 + $stick.height() );
    if ($(document).width() < 1500) {
          test = $('.Box').height(); /*520*/
    } else if ($(document).width() > 1500) {
        test = 300;
    }

        }

        allLoadBtn.forEach(node => {
          node.addEventListener('click', showMore)
        });

        $stick = $('.Box');
        $foot = $('.Agrees__container');
        margin = 20;
        offtop = $stick.offset().top;
        offbtm = $foot.offset().top - ( margin*2 + $stick.height() );
test = 20;
$( window ).resize(function() {
    if ($(document).width() > 1200) {
        stickyBox();
        }
  });
if ($(window).width() > 1200) {
    stickyBox();
    }
    function stickyBox() {
        $(window).scroll(function () {
            scrtop = $(window).scrollTop();
          if (scrtop > offtop-test && $stick.hasClass('natural')) {
            $stick.removeClass('natural').addClass('fixed').css('top', margin);
          }
          if (offtop > scrtop+test && $stick.hasClass('fixed')) {
            $stick.removeClass('fixed').addClass('natural').css('top', '140px');
          }
          if (scrtop > offbtm && $stick.hasClass('fixed')) {
            $stick.removeClass('fixed').addClass('bottom').css('top', offbtm+margin);
          }
          if (offbtm > scrtop && $stick.hasClass('bottom')) {
            $stick.removeClass('bottom').addClass('fixed').css('top', margin);
          }
        });
    }
        })


        /* $('.Form__input').keyup(function(e){
          if(e.target.value){
            $('label.error').css('z-index', '-2');
          } else {
            $('label.error').css('z-index', '2');
          }
        })
        */