
<!DOCTYPE html>
<html lang="pl">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
      <title>Carsmile</title>
      <link href="./dest/css/app.css" rel="stylesheet" type="text/css" />
      <link rel="shortcut icon" href="https://carsmile.pl/img/favicon/favicon.ico">


      <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PC37ZJK');</script>
<!-- End Google Tag Manager -->
   </head>
   <body>
      <!-- Google Tag Manager (noscript) -->
      <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PC37ZJK"
         height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <!-- End Google Tag Manager (noscript) -->
      <form id="LPform" action="./">
         <input type="hidden" name="selectedcar" value="<?= $carNameForm ?>" />

         <div class="main-wrapper">
            <div class="row main">
               <div class="grid-x grid-padding-x PhoneMarginMinus">
                  <div class="cell large-12 speciallarge-8 carSlideImage">
                     <div class="mobile-text-header">
                     <a href="#">
                     <img src="./dest/images/logo.svg" alt="" class="Header__logoImg" />
                     </a>
                     <h1 class="header-main-text">Już wkrótce skontaktujemy się z Tobą,</h1>
                     <h1>by pomóc Ci w wyborze nowego samochodu!</h1>
                     <a href="http://carsmile.pl" class="back-to-home">Zobacz pełną ofertę</a>

                     </div>
                  </div>
                  <div class="cell large-12 speciallarge-4 padding-less speciallarge relative" style="position: relative;">
                      <img src="./dest/images/bg-form-success.svg" class="bg-image" alt="">
                    <div class="Box success-shape">
                       <div class="BoxWithShadow form-head">
                          <div class="grid-x">
                             <div class="BlueBox cell xphone-12 large-12">
                                <div class="BlueBox__header1">
                                  <h2>Przeczytaj na blogu Carsmile:</h2>

                                  <div class="post-item">
                                    <date>12 lip 2018</date>
                                    <h3><span>Nowe auto za niewielkie pieniądze?</span><span>To możliwe! Wystarczy tylko...</span></h3>
                                    <article>
                                      Korzystanie z wymarzonego auta nie musi więc już oznaczać rozbicia domowego banku lub nadwyrężenia firmowego budżetu. Kredyt, leasing i abonament – każda z tych opcji ma swoją specyfikę. Na którą z nich warto się zdecydować?  
                                    </article>
                     <a href="https://carsmile.pl/blog/nowe-auto-taniej-niz-w-salonie" class="full-post">Dowiedz się więcej</a>
                                  </div>
                                  <div class="post-item">
                                    <date>1 lip 2018</date>
                                    <h3><span>Najchętniej kupowane auta</span><span>w Polsce w 2017 roku</span></h3>
                                    <article>
                                      Polacy mają coraz zasobniejsze portfele, a to przekłada się na wzrost sprzedaży aut, również tych prosto z salonu. Kluczowe kwestie przy wyborze pojazdu stanowią dla nas jakość, cena, wygląd i technologia. Oferta dostępnych aut jest bardzo szeroka, lecz słupki sprzedażowe niektórych modeli zdecydowanie górują nad konkurencją. Jakie auta wybieramy więc najchętniej?
                                    </article>
                     <a href="https://carsmile.pl/blog/najchetniej-kupowane-auta-w-polsce-w-2017-roku" class="full-post">Dowiedz się więcej</a>
                                  </div>

                                </div>
                                </div>
                                <!-- <p class="text">WYNEGOCJOWALIŚMY DLA CIEBIE:</p> -->
                                <!-- <img src="./dest/images/rabat.svg" alt="" class="BlueBox-img show-for-xphone"> -->
                             </div>
                          </div>
                       </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <footer class="Footer non-agress-padding">
            <div class="row">
               <div class="grid-x grid-padding-x">
                  <div class="cell">
                     <div class="menu-block">
                        <p class="title">Carsmile <span class="copyright">© 2018</span></p>
                        <p class="Footer-disclaimer">
                           Carsmile Spółka Akcyjna z siedzibą w Warszawie, ul. Prosta 51, 00-838 Warszawa, wpisana do rejestru przedsiębiorców prowadzonego przez Sąd Rejonowy dla m.st. Warszawy w Warszawie, XII Wydział Gospodarczy Krajowego Rejestru Sądowego, pod numerem KRS 0000724919, REGON 369367192, NIP 7010800013.
                        </p>
                        <p class="Footer-disclaimer">
                        Złożenie przez nas ostatecznej oferty zależy od Twojej zdolności do płacenia abonamentu obliczonej m.in. w oparciu o Twoje zarobki, zobowiązania oraz historię finansową.
                        </p>
                     </div>
                  </div>
               </div>
            </div>
         </footer>
      </form>
      <!-- All your html code -->
      <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
      <script src="<?= $tpl ?>/dest/js/validate.js"></script>
      <script src="<?= $tpl ?>/dest/js/main.js"></script>
   </body>
</html>
