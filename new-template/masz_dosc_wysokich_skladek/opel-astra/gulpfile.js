var gulp = require('gulp');
var cleanCSS = require('gulp-clean-css');
var $    = require('gulp-load-plugins')();
var babel = require('gulp-babel');
var minify = require('gulp-babel-minify');

// Jeden task do cleana!

gulp.task('sass', function () {
  return gulp.src('./app/sass/app.scss')
    .pipe($.sourcemaps.init())
    .pipe($.sass({
      includePaths:['node_modules/']
    })
    .on('error', $.sass.logError))
    .pipe($.autoprefixer({
      browsers: ['last 10 versions', 'ff 10', 'safari 6', 'ie >= 9']
    }))
    .pipe(cleanCSS({
      compatibility: 'ie8'
    }))
    .pipe($.sourcemaps.write('./dest/css/sourcemaps/'))
    .pipe(gulp.dest('./dest/css/'));
});

gulp.task('es6', function() {
  return gulp.src('./app/js/main.js')
        .pipe(babel())
        .pipe(minify({
          mangle: {
            keepClassName: true
          }
        }))
        .pipe(gulp.dest('./dest/js/'));
});


gulp.task('default', ['sass', 'es6'], function() {
  // Kompiluj SASSY
  gulp.watch(['./app/sass/**/*.scss'], ['sass']);
  // Import JavaScript
  gulp.watch(['./app/js/**/*.js'], ['es6']);

});
