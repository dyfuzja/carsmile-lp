<?php

use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

require '/home/zylion2/ftp/csm/vendor/autoload.php';

$tpl = '.';

$carNAME = "Nissan<br>Qashqai";
$carNameForm = "Nissan Qashqai";
$carPRICE = 745;
$promoImg = "/dest/images/promo_qashqai.png";
$promoSmallImg = "/dest/images/img_qashqai.jpg";

$client = new \GuzzleHttp\Client([
    'base_uri' => 'http://crm.carsmile.pl/api/',
    'headers'   => [
        'token' => 'G2jVr1a1yATpPzssfCkb8YFoFtnUGOOf66V3BnFcbsQGb061OHcpLp4ujccKvf',
        'name' => 'lp-marki',
        'Accept'    => 'application/json',
        'Content-Type' => 'application/json'
    ],
    'auth' => ['csm', 'csm123'],
]);

if( @$_POST['client_email'] ){
    if(@$_POST['client_nip']){
        $client_type = 'jdg';
    } else {
        $client_type = 'of';
    }
    $data = [
        'lead_type' => 'lt2',
        "lead_source" => "kampania",
        "client_type" => $client_type,
        "car_typ_code" => "122969-1",
        'first_name' => @$_POST['client_name'],
        'phone' => @$_POST['client_mobile'],
        'email' => @$_POST['client_email'],
        'nip' => @$_POST['client_nip'],
        'client_ip' => $_SERVER['REMOTE_ADDR'],
        'client_user_agent' => $_SERVER['HTTP_USER_AGENT'],
        "car_color" => 'czerwony',
        "product_type" => 'rent',
        "agreement_period" => 60,
        "self_payment" => 24500,
        'agreements' => [
            ['id' => 6, 'value' => (bool) $_POST['form_agree1'] ],
            ['id' => 7, 'value' => (bool) $_POST['form_agree2'] ],
            ['id' => 8, 'value' => (bool) $_POST['form_agree3'] ],
            ['id' => 9, 'value' => (bool) $_POST['form_agree4'] ]
        ]
    ];

    $body = (string)\GuzzleHttp\json_encode($data);
    // header('Content-Type: application/json');
    try {
        $response = $client->post('leadcreate', ['body' => $body]);

        echo (string)$response->getBody();
    } catch (ClientException $e) {
        echo (string)$e->getResponse()->getBody(true);
    } catch(ServerException $e) {
        echo (string)$e->getResponse()->getBody(true);
    }
/*    $lead_id = $modules->get('ContactForm')->createSubmission();
    $modules->get('ContactForm')->sendEmail( );
    echo $lead_id;*/
    die();
}


$response = $client->get('getagree');
$result = json_decode((string) $response->getBody(), true);
$agreements = [];
foreach($result['agreements'] as $item) {
    if(in_array($item['id'], [6, 7, 8, 9, 10])) {
        $agreements[$item['id']] = $item;
    }
}
