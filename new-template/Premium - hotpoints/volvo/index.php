<?php include('_carSettings.php');
   $tpl = '.';

   $carNAME = "Nissan<br>Qashqai";
   $carNameForm = "Nissan Qashqai";
   $carPRICE = 745;
   $promoImg = "/dest/images/promo_qashqai.png";
   $promoSmallImg = "/dest/images/img_qashqai.jpg";
    ?>
<!DOCTYPE html>
<html lang="pl">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
      <title>Carsmile</title>
      <link href="dest/css/app.css" rel="stylesheet" type="text/css" />
      <link rel="shortcut icon" href="https://carsmile.pl/img/favicon/favicon.ico">
      <script>
         dataLayer = window.dataLayer || [{
             'st.pageCategory':'LandingPage',
             'st.desc':'contact data',
             // 'offerType':<dla-mnie|dla-firmy>,
             // 'financeType':<najem|leasing>,
             // 'carCategory': '???',
             'carBrand': 'Renault',
             'carType': 'Clio',
             'carBrandTypeID': '131492-1'
         }];

      </script>
     <!-- Google Tag Manager
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PC37ZJK');</script>
End Google Tag Manager -->
   </head>
   <body>
      <!-- Google Tag Manager (noscript) -->
      <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PC37ZJK"
         height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <!-- End Google Tag Manager (noscript) -->
         <input type="hidden" name="selectedcar" value="<?= $carNameForm ?>" />
         <header class="Header" >
            <div class="row">
               <div class="grid-x grid-padding-x align-middle phone-reverse">
                     <a href="#">
                     <img src="dest/images/logo.svg" alt="" class="Header__logoImg" />
                     </a>
               </div>
            </div>
         </header>
         <div class="main-wrapper">
         <div class="mobile-text-header">
                     <h1 class="header-main-text">
                       <span class="header-brand">Volvo XC60</span>
                        <span class="header-from">już od</span>
                        <span class="header-price">3563</span>
                        <span class="header-pln">PLN /mies.</span>
                     </h1>
                     </div>
                  <img class="head-img" src="dest/images/volvo.jpg" alt="Volvo XC60">

                  <div class="points-wrapper">
                    <div class="points">        height: 100vh;


                    <div class="single-point-wrapper">
                    <button class="point">+</button>
                    <div class="content-box">
                      <div class="close-popup"></div>
                    <article>
     <img src="dest/images/interior-side.jpg" alt="">
                        <h2>Tankowanie sił za pomocą pakietu komfortowego sterowania ENERGIZING</h2>
                          <p>
                          Nowa Klasa C nie tylko zachwyca swoją siłą, lecz może także sprzyjać dobremu samopoczuciu kierowcy: dzięki opcjonalnemu sterowaniu komfortowemu ENERGIZING. Światła, dźwięki i zapachy można łączyć we wzajemnie kombinacje działające na pasażerów odprężająco lub ożywczo. Włączone do pakietu nastrojowe oświetlenie może za pomocą 64 kolorów inscenizować najpiękniejsze oświetlenie wnętrza samochodu.
                        </p>
                      </article>
                    </div>
                    </div>


                    <div class="single-point-wrapper">
                    <button class="point">+</button>
                    <div class="content-box">
                    <div class="close-popup"></div>
                    <article>
     <img src="dest/images/interior-side.jpg" alt="">
                        <h2>Tankowanie sił za pomocą pakietu komfortowego sterowania ENERGIZING</h2>
                          <p>
                          Nowa Klasa C nie tylko zachwyca swoją siłą, lecz może także sprzyjać dobremu samopoczuciu kierowcy: dzięki opcjonalnemu sterowaniu komfortowemu ENERGIZING. Światła, dźwięki i zapachy można łączyć we wzajemnie kombinacje działające na pasażerów odprężająco lub ożywczo. Włączone do pakietu nastrojowe oświetlenie może za pomocą 64 kolorów inscenizować najpiękniejsze oświetlenie wnętrza samochodu.
                        </p>
                      </article>
                    </div>
                    </div>

                    <div class="single-point-wrapper">
                    <button class="point">+</button>
                    <div class="content-box">
                    <div class="close-popup"></div>
                    <article>
     <img src="dest/images/interior-side.jpg" alt="">
                        <h2>Tankowanie sił za pomocą pakietu komfortowego sterowania ENERGIZING</h2>
                          <p>
                          Nowa Klasa C nie tylko zachwyca swoją siłą, lecz może także sprzyjać dobremu samopoczuciu kierowcy: dzięki opcjonalnemu sterowaniu komfortowemu ENERGIZING. Światła, dźwięki i zapachy można łączyć we wzajemnie kombinacje działające na pasażerów odprężająco lub ożywczo. Włączone do pakietu nastrojowe oświetlenie może za pomocą 64 kolorów inscenizować najpiękniejsze oświetlenie wnętrza samochodu.
                        </p>
                      </article>
                    </div>
                    </div>

                    <div class="single-point-wrapper">
                    <button class="point">+</button>
                    <div class="content-box">
                    <div class="close-popup"></div>
                    <article>
     <img src="dest/images/interior-side.jpg" alt="">
                        <h2>Tankowanie sił za pomocą pakietu komfortowego sterowania ENERGIZING</h2>
                          <p>
                          Nowa Klasa C nie tylko zachwyca swoją siłą, lecz może także sprzyjać dobremu samopoczuciu kierowcy: dzięki opcjonalnemu sterowaniu komfortowemu ENERGIZING. Światła, dźwięki i zapachy można łączyć we wzajemnie kombinacje działające na pasażerów odprężająco lub ożywczo. Włączone do pakietu nastrojowe oświetlenie może za pomocą 64 kolorów inscenizować najpiękniejsze oświetlenie wnętrza samochodu.
                        </p>
                      </article>
                    </div>
                    </div>

                    </div>
                  </div>

            </div>

            <div class="bottom-wrapper">
              <div class="row main">
     <div class="grid-x grid-padding-x PhoneMarginMinus">

            <div class="cell large-12 speciallarge-8 padding-less speciallarge relative bottom-content" style="position: relative;">
            <h2>
     <span>Carsmile.</span>
     <span>Wszystkie marki premium</span>
     <span>w jednym miejscu</span>
     </h2>

     <article>
     <h4>Pełna przewidywalność kosztów.</h4>
Chcesz cieszyć się eksploatacją samochodu, ale bez konieczności przejmowania się kosztami związanymi z cyklicznymi przeglądami w autoryzowanym serwisie. Nie chcesz też płacić za wymianę klocków hamulcowy, piór wycieraczek czy żarówek. Wszystkim tym zajmiemy się my, a Ty ciesz się użytkowaniem auta i świętym spokojem.
     </article>

     <article>
     <h4>Pełny pakiet ubezpieczeniowy</h4>
     Nie musisz przejmować się wyszukiwaniem i porównywaniem ofert ubezpieczycieli. Decydując się na najem długoterminowy w Carsmile otrzymujesz pełny pakiet ubezpieczeń OC+AC+NNW, a nawet Assistance w cenie. Możesz też zgłaszać szkody komunikacyjne przy pomocy swojego konta na naszej stronie.
     </article>

     <article>
     <h4>Cała obsługa związana z wymianą opon</h4>
     Decydując się na najem w Carsmile w miesięcznym abonamentu masz zawarty nie tylko komplet opon zimowych. My bierzemy na siebie również koszty ich przechowywania oraz wymiany. Nawet umówimy dla Ciebie wizytę w zakładzie wulkanizacyjnym
     </article>
            </div>
              <div class="cell large-12 speciallarge-4 padding-less speciallarge bottom-form relative" style="position: relative;">
                <div class="Box">
                    <div class="BoxWithShadow BoxForForm">
                       <div class="showbox" style="display: none">
                          <div class="loader"></div>
                          <p class="loading_info">Trwa przesyłanie formularza</p>
                       </div>
                       <div class="success" style="display: none">
                          <p class="loading_inf successInfo"><h2 class="ty-header">Dziękujemy.</h2> <br/><h3 class="ty-header">Już wkrótce skontaktujemy się z Tobą by pomóc Ci w wyborze nowego samochodu!</h3></p>
                       </div>
                       <div class="Form">
                          <p class="headtext">Wypełnij formularz</p>
                          <p class="undertext">i dowiedz się jak tani może być nowy samochód</p>
                          <div class="Form-wrapper">
                          <form id="LPform" action="./">

                             <div class="Form__input-wrapper">
                                <input autocomplete="off" type="text" name="client_name" class="Form__input client_name">
                                <label class="Form__input-label client_name-label">Imię</label>
                                <img src="<?= $tpl ?>/dest/images/tickYES.svg" alt="" class="inputValid">
                             </div>
                             <div class="grid-x grid-margin-x">
                                <div class="cell xphone-auto">
                                   <div class="Form__input-wrapper">
                                      <input autocomplete="off" type="text" name="client_mobile" class="Form__input client_mobile">
                                      <label class="Form__input-label client_mobile-label">Telefon</label>
                                      <img src="<?= $tpl ?>/dest/images/tickYES.svg" alt="" class="inputValid">
                                   </div>
                                </div>
                                <div class="cell xphone-auto lessLeft">
                                   <div class="Form__input-wrapper">
                                      <input autocomplete="off" type="text" name="client_email" class="Form__input client_email">
                                      <label class="Form__input-label client_email-label">E-mail</label>
                                      <img src="<?= $tpl ?>/dest/images/tickYES.svg" alt="" class="inputValid">
                                   </div>
                                </div>
                             </div>
                             <div class="grid-x grid-margin-x check-company-normalize">
                                <div class="cell xphone-auto Agrees terms-align agreeAndButton">
                                   <div class="Form__input-wrapper terms company-check Agrees">
                                      <!-- <label for="checkNIP" class="checkNIP-label">
                                         <span class="checkbox checkbox-large"><span class="checked"></span></span> -->


                                         <!-- <input type="checkbox" hidden="hidden" id="username" value="test"> -->

                                      <input type="checkbox" id="checkNIP" name="checkNIP" value="true">


                                         <label class="switch" for="checkNIP" class="checkNIP-label"></label>


                                         <span class="terms-text company-terms-text nip-padd">Prowadzisz firmę?</span>
                                         <div class="error formFirst"></div>
                                   </div>
                                </div>
                                <div class="cell xphone-auto lessLeft nip-normalize">
                                   <div class="Form__input-wrapper">
                                      <input autocomplete="off" type="text" name="client_nip" class="Form__input client_nip" id="client_nip">
                                      <label class="Form__input-label client_nip_label hide-label">NIP</label>
                                      <img src="<?= $tpl ?>/dest/images/tickYES.svg" alt="" class="inputValid">
                                   </div>
                                </div>
                             </div>
                             <div class="agreeAndButton">
                                <div class="Agrees">
                                   <div class="cell auto">
                                      <div class="terms check-all-wrapper">
                                         <div class="company-check">
                                            <!-- <input type="checkbox" id="checkAll" name="zaznaczAll" value="true">
                                            <label for="checkAll"> -->


                                            <input type="checkbox" id="checkAll" name="checkAll" value="true">
                                         <label class="switch switch-agrees" for="checkAll" class="checkNIP-label"></label>


<!-- <span class="checkbox checkbox-large"><span class="checked"></span></span> -->

                                               <span class="terms-text"><b>Zaznacz wszystkie zgody</b></span>
                                               <div class="error formFirst"></div>
                                         </div>
                                      </div>
                                   </div>
                                </div>
                                <button type="submit" class="Form__SendBtn">
                                Wyślij
                                </button>
                             </div>
                             </form>
                          </div>
                       </div>
                    </div>
                 </div>
               <img src="dest/images/volvo.jpg" alt="">
               </div>
             </div>
            </div>

         <div class="Agrees Agrees__container BoxWithShadow BoxShadowAgrees">
            <div class="formSendContainer" style="display: none"></div>
            <div class="row">
               <div class="grid-x grid-padding-x">
                  <div class="cell xphone-4">
                     <div class="terms">
                        <input type="checkbox" agree="general" id="form_agree1" name="form_agree1" value="true" <?= $agreements[6]['required'] ? 'required' : '' ?>>
                        <label for="form_agree1">
                           <span class="checkbox"><span class="checked"></span></span>
                           <span class="terms-text"><?= $agreements[6]['content'] ?></span>
                           <div class="error errorPlacement"></div>
                        </label>
                     </div>
                     <div class="terms">
                        <input type="checkbox" agree="general" id="form_agree2" name="form_agree2" value="true" <?= $agreements[7]['required'] ? 'required' : '' ?>>
                        <label for="form_agree2">
                           <span class="checkbox"><span class="checked"></span></span>
                           <span class="terms-text"><?= $agreements[7]['content'] ?></span>
                           <p class="error errorPlacement"></p>
                        </label>
                     </div>
                  </div>
                  <div class="cell xphone-4">
                     <div class="terms">
                        <input type="checkbox" agree="general" id="form_agree3" name="form_agree3" value="true" <?= $agreements[8]['required'] ? 'required' : '' ?>>
                        <label for="form_agree3">
                           <span class="checkbox"><span class="checked"></span></span>
                           <span class="terms-text"><?= $agreements[8]['content'] ?></span>
                           <p class="error errorPlacement"></p>
                        </label>
                     </div>
                  </div>
                  <div class="cell xphone-4">
                     <div class="terms">
                        <input type="hidden" name="form_agree4" value="false">
                        <input type="checkbox" agree="general" id="form_agree4" name="form_agree4" value="true" <?= $agreements[9]['required'] ? 'required' : '' ?>>
                        <label for="form_agree4">
                           <span class="checkbox"><span class="checked"></span></span>
                           <span class="terms-text"><?= $agreements[9]['content'] ?></span>
                           <p class="error errorPlacement"></p>
                        </label>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="Rodo_disclaimer">
                  <div class="grid-x grid-padding-x">
                     <div class="cell auto rodo_text">
                        <?= $agreements[10]['content'] ?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <footer class="Footer">
            <div class="row">
               <div class="grid-x grid-padding-x">
                  <div class="cell">
                     <div class="menu-block">
                        <p class="title">Carsmile <span class="copyright">© 2018</span></p>
                        <p class="Footer-disclaimer">
                           Carsmile Spółka Akcyjna z siedzibą w Warszawie, ul. Prosta 51, 00-838 Warszawa, wpisana do rejestru przedsiębiorców prowadzonego przez Sąd Rejonowy dla m.st. Warszawy w Warszawie, XII Wydział Gospodarczy Krajowego Rejestru Sądowego, pod numerem KRS 0000724919, REGON 369367192, NIP 7010800013.
                        </p>
                        <p class="Footer-disclaimer">
                        Złożenie przez nas ostatecznej oferty zależy od Twojej zdolności do płacenia abonamentu obliczonej m.in. w oparciu o Twoje zarobki, zobowiązania oraz historię finansową.
                        </p>
                     </div>
                  </div>
               </div>
            </div>
         </footer>
      <!-- All your html code -->
      <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
      <script src="<?= $tpl ?>/dest/js/validate.js"></script>
      <script src="<?= $tpl ?>/dest/js/main.js"></script>
   </body>
</html>
