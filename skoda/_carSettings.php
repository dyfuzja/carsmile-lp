<?php
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

require  './vendor/autoload.php';

$tpl = '.';

$carNAME = "Skoda<br>Fabia";
$carNameForm = "Skoda Fabia";
$carPRICE = 599;
$promoImg = "/dest/images/promo_fabia.png";
$promoSmallImg = "/dest/images/img_fabia.jpg";


$client = new \GuzzleHttp\Client([
    'base_uri' => 'http://crm.carsmile.pl/api/',
    'headers'   => [
        'token' => 'TgPAiJqEYdQLC9Lb53RnCVGp9HVPHf5HJvYU2ojQgjj7BJKz1cQySa6tU3mkMZ',
        'name' => 'universal',
        'Accept'    => 'application/json',
        'Content-Type' => 'application/json'
    ],
    'auth' => ['csm', 'csm123'],
]);

if( @$_POST['client_email'] ){
    $data = [
        'lead_type' => 'lt2',
        "lead_source" => "kampania",
        "client_type" => "of",
        "car_typ_code" => "108777-1",
        'first_name' => $_POST['client_name'],
        'phone' => $_POST['client_mobile'],
        'email' => $_POST['client_email'],
        'client_ip' => $_SERVER['REMOTE_ADDR'],
        'client_user_agent' => $_SERVER['HTTP_USER_AGENT'],
        "car_color" => 'czerwony',
        "product_type" => 'rent',
        "agreement_period" => 36,
        "self_payment" => 4500,
        'agreements' => [
            ['id' => 6, 'value' => (bool) $_POST['form_agree1']],
            ['id' => 7, 'value' => (bool) $_POST['form_agree2']],
            ['id' => 8, 'value' => (bool) $_POST['form_agree3']],
            ['id' => 9, 'value' => (bool) $_POST['form_agree4']]
        ]
    ];

    $body = (string)\GuzzleHttp\json_encode($data);
    header('Content-Type: application/json');
    
    try {
        $response = $client->post('leadcreate', ['body' => $body]);

        echo (string)$response->getBody();
    } catch (ClientException $e) {
        echo (string)$e->getResponse()->getBody(true);
    } catch(ServerException $e) {
        echo (string)$e->getResponse()->getBody(true);
    }
    /*    $lead_id = $modules->get('ContactForm')->createSubmission();
        $modules->get('ContactForm')->sendEmail( );
        echo $lead_id;*/
    die();
}


$response = $client->get('getagree');
$result = json_decode((string) $response->getBody(), true);
$agreements = [];
foreach($result['agreements'] as $item) {
    if(in_array($item['id'], [6, 7, 8, 9, 10])) {
        $agreements[$item['id']] = $item;
    }
}