<?php
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

require './vendor/autoload.php';

$tpl = '.';

$client = new \GuzzleHttp\Client([
    'base_uri' => 'http://crm.carsmile.pl/api/',
    'headers'   => [
        'token' => 'Qjz7cCrer9IVPjOUgvZstBER2foUNRk4CpIABCcC5E7XcTWDg3qDEI1TEccUTQ',
        'name' => 'autotrader',
        'Accept'    => 'application/json',
        'Content-Type' => 'application/json'
    ],
    'auth' => ['csm', 'csm123'],
]);

if( @$_POST['client_email'] ){
    $data = [
        'lead_type' => "lt5",
        "client_type" => "of",
        "lead_source" => "kampania",
        "campaign" => "autotrader",
        "car_type" => "osob",
        // "car_typ_code" => "",
        'first_name' => $_POST['client_name'],
        'phone' => $_POST['client_mobile'],
        'email' => $_POST['client_email'],
        'comment' => $_POST['comment'],
        "car_color" => "",
        "product_type" => "",
        "agreement_period" => 0,
        "self_payment" => 0,
        // 'agreements' => [
        //     ['id' => 6, 'value' => (bool) $_POST['form_agree1']],
        //     ['id' => 7, 'value' => (bool) $_POST['form_agree2']],
        //     ['id' => 8, 'value' => (bool) $_POST['form_agree3']],
        //     ['id' => 9, 'value' => (bool) $_POST['form_agree4']]
        // ]
    ];

    $body = (string)\GuzzleHttp\json_encode($data);
    header('Content-Type: application/json');
    
    try {
        $response = $client->post('leadcreate', ['body' => $body]);

        echo (string)$response->getBody();
    } catch (ClientException $e) {
        echo (string)$e->getResponse()->getBody(true);
    } catch(ServerException $e) {
        echo (string)$e->getResponse()->getBody(true);
    }
    /*    $lead_id = $modules->get('ContactForm')->createSubmission();
        $modules->get('ContactForm')->sendEmail( );
        echo $lead_id;*/
    die();
}


$response = $client->get('getagree');
$result = json_decode((string) $response->getBody(), true);
// $agreements = [];
// foreach($result['agreements'] as $item) {
//     if(in_array($item['id'], [6, 7, 8, 9, 10])) {
//         $agreements[$item['id']] = $item;
//     }
// }