$(document).ready(function() {

    $('#checkAll').click(function () {

        if ( $(this).prop('checked') === true ) {

            $('.terms input[type=checkbox]').prop('checked', true);

        } else {

            $('.terms input[type=checkbox]').prop('checked', false);

        }

    })

    $('.Form__input').on('input', function() {

        $(this).val() ? $(this).next('.Form__input-label').addClass('hasValue') : $(this).next('.Form__input-label').removeClass('hasValue');

    })

    $('.Form__input').on('blur', function(e) {

        var validator = $( "#LPform" ).validate();
        validator.element( this );

    });

    $.validator.addMethod("emailCheck", function( value, element ) {
        return this.optional( element ) || /^.+@.+\.[A-Za-z]{2,}$/.test( value );
        }, "Niepoprawny adres e-mail");
    
    $.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-zżźćńółęąśŻŹĆĄŚĘŁÓŃ\s]+$/i.test(value);
        }, "Proszę wpisać tylko litery");
    
    $.validator.addMethod("digitsonly", function(value, element) {
        return this.optional(element) || /^[0-9]+$/i.test(value);
        }, "Proszę wpisać tylko cyfry");
    
    $.validator.addMethod("phonePL", function(phonenr, element) {
        phonenr = phonenr.replace(/\s+/g, "");
        return this.optional(element) || phonenr.length > 8 && phonenr.length < 10 &&
            phonenr.match(/^(0-?)?(\([0-9]\d{2}\)|[0-9]\d{2})-?[0-9]\d{2}-?\d{3,4}$/);
    
        }, "Niepoprawny numer telefonu");


        if ( $('#LPform').length ) {

            var $form = $('#LPform');
    
            $form.validate({
                rules: {
                    client_email: {
                        required: true,
                        emailCheck: true
                    },
                    client_mobile: {
                        required: true,
                        phonePL: true
                    },
                    client_name: {
                        required: true,
                        lettersonly: true
                    },
                    form_agree1: "required",
                    form_agree2: "required",
                    form_agree3: "required"
                },
                messages: {
                    client_email: {
                        required: "Pole wymagane"
                    },
                    client_mobile: {
                        required: "Pole wymagane",
                        phonePL: "Niepoprawny nr telefonu"
                    },
                    client_name: {
                        required: "Pole wymagane",
                    },
                    form_agree1: "Pole wymagane",
                    form_agree2: "Pole wymagane",
                    form_agree3: "Pole wymagane"

                },
                errorPlacement: function(error, element) {


                    var errorCheckboxPlace = $(element).next().find('.errorPlacement');

                    if ( $(element).parent().hasClass('terms') ) {

                        error.appendTo( errorCheckboxPlace );

                    } else {
                        error.insertBefore(element);
                    }

                },
                submitHandler: function(form) {

                    var $showbox = $('.showbox');
                    var $successSent = $('.successInformation');
                    var $error = $('.errorInformation');
                    var $formContainerAgrees = $('.formSendContainer');
                    $showbox.show();
                    $formContainerAgrees.show();

                    $.post( $(form).attr('action'), $(form).serialize(), function (data) {

                        // {"saved":1,"id_lead_www":666,"error":null} = data;
                        var dataResponse = null;

                        if (typeof data === "string") {
                            dataResponse = JSON.parse(data);
                        } else {
                            dataResponse = data;
                        }

                        if( dataResponse.saved ){

                            $showbox.hide();
                            $successSent.show();

                            if(window['dataLayer']) dataLayer.push({
                                'event':'STevent',
                                'eventCategory':'CCLead',
                                'eventAction':'Form button',
                                'eventLabel':'Wyslij',
                                'leadcm': dataResponse.id_lead_www
                            });

                        }

                        if (!!data.error) {

                                $showbox.hide();
                                $error.show();

                        }

                    } );
             
                    return false;

                    console.log('Wysłano formularz');
                },
                invalidHandler: function(form) {

                    // console.log('Wystapil blad przy walidacji');
                }
            }).settings.ignore = [];
    
        }

})