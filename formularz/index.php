<?php include('_carSettings.php'); ?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">

    <title>Formularz - Carsmile</title>

    <link href="<?= $tpl ?>/dest/css/app.css" rel="stylesheet" type="text/css" />

        <script>

            dataLayer = window.dataLayer || [{
                // 'st.pageCategory':'LandingPage',
                'st.desc':'contact data',
                // 'offerType':<dla-mnie|dla-firmy>,
                // 'financeType':<najem|leasing>,
                // 'carTypeId':'126846-1',
                // // 'carCategory': '???',
                // 'carBrand': 'Nissan',
                // 'carType': 'Qashqai',
                // 'carBrandTypeID': '126846-1'
            }];

        </script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer',' GTM-PC37ZJK');</script>
    <!-- End Google Tag Manager -->

</head>
<body>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PC37ZJK"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <form id="LPform">

    <div class="row main">
        <div class="grid-x grid-padding-x PhoneMarginMinus">
            <div class="cell">
                <div class="Box">
                    <div class="BoxWithShadow BoxForForm">
                        <div class="showbox" style="display: none">
                            <div class="loader"></div>
                            <p class="loading_info">Trwa przesyłanie formularza</p>
                        </div>
                        <div class="success successInformation" style="display: none">
                            <img src="<?= $tpl ?>/dest/images/tickYES.png" alt="" class="successForm">
                            <p class="loading_info successInfo">Dziękujemy. <br/>Formularz został przesłany.</p>
                        </div>
                        <div class="success errorInformation" style="display: none">
                            <p class="iks">&times;</p>
                            <p class="loading_info error">Wystąpił błąd. Formularz nie został przesłany.</p>
                        </div>
                        <div class="Form">
                            <p class="headtext">Wypełnij formularz</p>

                            <div class="Form-wrapper">
                                <div class="Form__input-wrapper">
                                    <input autocomplete="off" type="text" name="client_name" class="Form__input">
                                    <label class="Form__input-label">Imię</label>
                                    <img src="<?= $tpl ?>/dest/images/tickYES.svg" alt="" class="inputValid">
                                </div>
                                <div class="grid-x grid-margin-x">
                                    <div class="cell xphone-auto lessRight">
                                        <div class="Form__input-wrapper">
                                            <input autocomplete="off" type="text" name="client_mobile" class="Form__input">
                                            <label class="Form__input-label">Telefon</label>
                                            <img src="<?= $tpl ?>/dest/images/tickYES.svg" alt="" class="inputValid">
                                        </div>
                                    </div>
                                    <div class="cell xphone-auto lessLeft">
                                        <div class="Form__input-wrapper">
                                            <input autocomplete="off" type="text" name="client_email" class="Form__input">
                                            <label class="Form__input-label">E-mail</label>
                                            <img src="<?= $tpl ?>/dest/images/tickYES.svg" alt="" class="inputValid">
                                        </div>
                                    </div>
                                </div>
                                <div class="Form__input-wrapper">
                                    <textarea autocomplete="off" type="text" rows="5" name="comment" class="Form__input commentTextarea"></textarea>
                                    <label class="Form__input-label">Komentarz</label>
                                    <img src="<?= $tpl ?>/dest/images/tickYES.svg" alt="" class="inputValid">
                                </div>
                                <div class="agreeAndButton">
                                    <!-- <div class="Agrees">
                                        <div class="cell">
                                            <div class="terms">
                                                <input type="checkbox" id="checkAll" name="zaznaczAll" value="true">
                                                <label for="checkAll">
                                                    <span class="checkbox"><span class="checked"></span></span>
                                                    <span class="terms-text"><b>Zaznacz wszystkie</b></span>
                                                    <div class="error formFirst"></div>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="cell xphone-4">
                                            <div class="terms">
                                                <input type="checkbox" id="form_agree1" name="form_agree1" value="true" <?= $agreements[6]['required'] ? 'required' : '' ?>>
                                                <label for="form_agree1">
                                                    <span class="checkbox"><span class="checked"></span></span>
                                                    <span class="terms-text"><?= $agreements[6]['content'] ?></span>
                                                    <div class="error errorPlacement"></div>
                                                </label>
                                            </div>
                                            <div class="terms">
                                                <input type="checkbox" id="form_agree2" name="form_agree2" value="true" <?= $agreements[7]['required'] ? 'required' : '' ?>>
                                                <label for="form_agree2">
                                                    <span class="checkbox"><span class="checked"></span></span>
                                                    <span class="terms-text"><?= $agreements[7]['content'] ?></span>
                                                    <p class="error errorPlacement"></p>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="cell xphone-4">
                                            <div class="terms">
                                                <input type="checkbox" id="form_agree3" name="form_agree3" value="true" <?= $agreements[8]['required'] ? 'required' : '' ?>>
                                                <label for="form_agree3">
                                                    <span class="checkbox"><span class="checked"></span></span>
                                                    <span class="terms-text"><?= $agreements[8]['content'] ?></span>
                                                    <p class="error errorPlacement"></p>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="cell xphone-4">
                                            <div class="terms">
                                                <input type="hidden" name="form_agree4" value="false">
                                                <input type="checkbox" id="form_agree4" name="form_agree4" value="true" <?= $agreements[9]['required'] ? 'required' : '' ?>>
                                                <label for="form_agree4">
                                                    <span class="checkbox"><span class="checked"></span></span>
                                                    <span class="terms-text"><?= $agreements[9]['content'] ?></span>
                                                    <p class="error errorPlacement"></p>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="Rodo_disclaimer">
                                            <div class="grid-x grid-padding-x">
                                                <div class="cell auto rodo_text">
                                                    <?= $agreements[10]['content'] ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->
                                    <button type="submit" class="Form__SendBtn">
                                        Wyślij
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="Footer">
        <div class="row">
            <div class="grid-x grid-padding-x">
                <div class="cell">
                    <div class="menu-block">
                        <p class="title">Carsmile <span class="copyright">© 2018</span></p>
                        <p class="Footer-disclaimer">
                            Carsmile Spółka Akcyjna z siedzibą w Warszawie, ul. Prosta 51, 00-838 Warszawa, wpisana do rejestru przedsiębiorców prowadzonego przez Sąd Rejonowy dla m.st. Warszawy w Warszawie, XII Wydział Gospodarczy Krajowego Rejestru Sądowego, pod numerem KRS 0000724919, REGON 369367192, NIP 7010800013.
                        </p>
                        <p class="Footer-disclaimer">
                            Wysokość rabatu zależy od wybranego samochodu i wpływa na niższą ratę najmu. Ostateczna wysokość czynszu może się różnić w zależności od okresu najmu i uzgodnionego wkładu własnego.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    </form>

    <!-- All your html code -->
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script src="<?= $tpl ?>/dest/js/validate.js"></script>
    <script src="<?= $tpl ?>/dest/js/main.js"></script>
</body>
</html>